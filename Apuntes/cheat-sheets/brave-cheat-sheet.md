---
date: 2023-05-17T12:18
title: Brave Cheat Sheet
author: AlbertoVf
tags: ['brave', 'browser', 'keys']
resume: Atajos por defecto de Brave
---

# Brave Cheat Sheet

## Tabs y Ventanas

- `Ctrl + T`: Nuevo Tab.
<!-- - `Ctrl + Shift + P`: Nueva Tab privada. -->
<!-- - `Ctrl + Shift + S`: Nueva Session Tab. -->
- `Ctrl + N`: Nueva ventana.
- `Ctrl + Q`: Quit Brave.
- `Ctrl + W`: Cerrar Tab.
- `Ctrl + Shift + W`: Cerrar ventana.
- `Ctrl + F4`: Cerrar ventana.
- `Ctrl + M`: Minimizar.
- `Ctrl + Shift + T`: Reabrir el ultimo tab cerrado.
- `Ctrl + Alt + Shift + T`: Reabrir la ultima ventana cerrada.
- `F11`: Cambiar a vista de pantalla completa.
- `Ctrl + 1 ... 9`: Ir al tab [1 .. 9].
- `Ctrl + Shift + ]` / `Ctrl + Alt + Arrow Right` / `Ctrl + PageDown` / `Ctrl + Tab`: Cambiar al siguiente Tab.
- `Ctrl + Shift + [` / `Ctrl + Alt + Arrow Left` / `Ctrl + PageUp` / `Ctrl + Shift + Tab`: Cambiar al anterior Tab.

## Navegacion de Pagina

- `F6`: Focus URL Bar.
- `Alt + D`: Focus URL Bar.
- `Ctrl + L`: Focus URL Bar.
- `Ctrl + Shift + H`: Ir a  Home.
- `Ctrl + F` /: Buscar en la pagina.
- `Ctrl + G`: Active tab Find Next.
- `Ctrl + Shift + G`: Active tab Find Previous.
- `Ctrl + Shift + +`: Acercar.
- `Ctrl + Shift + -`: Alejar.
- `Ctrl + 0`: Reestablecer zoom.
- `Esc`: Parar la carga.
- `Ctrl + R`: Recargar pagina.
- `F5`: Recargar pagina.
- `Ctrl + Shift + R` / `Ctrl + F5`: Limpiar recarga.
- `Alt + Left`: Volver a cargar la pagina anterior.
- `Alt + Right`: Cargar la pagina siguiente.
- `Ctrl + [`: Go Back.
- `Ctrl + ]`: Go Forward.
- `Ctrl + Z`: Deshacer.
- `Ctrl + Shift + Z`: Volver a hacer.
- `Ctrl + X`: Cortar.
- `Ctrl + C`: Copiar.
- `Ctrl + V`: Pegar.
- `Ctrl + Shift + V`: Pegar sin formato.

## Marcadores, historial, ajustes y herramientas

- `Ctrl + D`: Añadir/editar marcador.
- `Ctrl + Shift + O`: Gestor de marcadores.
- `Ctrl + J`: Abrir descargas.
- `Ctrl + Y`: Abrir historial.
- `Ctrl + Shift + Delete`: Limpiar datos.
- `Ctrl + O`: Abrir archivo.
- `Ctrl + S`: Guardar pagina como.
- `Ctrl + P`: Imprimir.
- `Ctrl + Alt + U`: Ver codigo.
- `Ctrl + Alt + J` / `Ctrl + Shift + I`: Herramientas del desarrollador.
