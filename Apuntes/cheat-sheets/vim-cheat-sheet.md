---
date: 2023-05-10T12:00
title: Vim Cheat Sheet
author: AlbertoVf
tags: ['vim', 'terminal', 'keys']
resume: Combinacion de teclas usadas en vim
---

# Vim Cheat Sheet

## Global

- `:h[elp] keyword`: abrir ayuda para palabra clave
- `:sav[eas] file`: guardar archivo como
- `:clo[se]`: cerrar archivo actual
- `:ter[minal]`: abre una ventana de terminal
- `K`: abrir página del manual (man page) de la palabra bajo el cursor

> `vimtutor` en terminal para aprender vim

## Mover el cursor

- `h`: mover el cursor hacia la izquierda
- `j`: mover el cursor hacia abajo
- `k`: mover el cursor hacia arriba
- `l`: mover el cursor hacia la derecha
- `H`: moverse hacia la parte superior de la pantalla
- `M`: moverse hacia el centro de la pantalla
- `L`: moverse hacia la parte inferior de la pantalla
- `w`: saltar hasta el inicio de la siguiente palabra
- `W`: saltar hasta el inicio de la siguiente palabra (incluso con puntuación)
- `e`: saltar hasta el final de la siguiente palabra
- `E`: saltar hasta el final de la siguiente palabra (incluso con puntuación)
- `b`: regresar al inicio de una palabra
- `B`: regresar al inicio de una palabra (incluso con puntuación)
- `%`: ir al carácter asociado (pares por defecto: '()', '{}', '[]' - usar :h matchpairs en vim para más información)
- `0`: saltar al inicio de la línea
- `^`: saltar al primer carácter no-espacio de la línea
- `$`: saltar al final de la línea
- `g_`: saltar al último carácter no-espacio de la línea
- `gg`: ir a la primera línea del documento
- `G`: ir a la última línea del documento
- `5gg` or `5G`: ir a la línea 5
- `fx`: saltar a la siguiente ocurrencia del carácter x
- `tx`: saltar a la ocurrencia previa del carácter x
- `Fx`: saltar a la aparición anterior del carácter x
- `Tx`: saltar a después de la aparición anterior del carácter x
- `;`: repetir el movimiento anterior de f, t, F o T
- `,`: repetir el movimiento anterior de f, t, F o T, hacia atrás
- `}`: saltar al párrafo siguiente (o función/bloque, en modo edición)
- `{`: saltar al párrafo previo (o función/bloque, en modo edición)
- `zz`: centrar el cursor en la pantalla
- `Ctrl + e`: mover la pantalla hacia abajo una línea (sin mover el cursor)
- `Ctrl + y`: mover la pantalla una línea hacia arriba (sin mover el cursor)
- `Ctrl + b`: retroceder una pantalla
- `Ctrl + f`: avanzar una pantalla
- `Ctrl + d`: avanzar cursor media pantalla
- `Ctrl + u`: retroceder cursor media pantalla

> **Tip**: Escribir un número antes del comando de movimiento para repetirlo. Por ejemplo, 4j mueve el cursor hacia abajo 4 líneas.

## Modo insertar - insertar/agregar texto

- `i`: insertar antes del cursor
- `I`: insertar al comienzo de la línea
- `a`: insertar después del cursor
- `A`: insertar al final de la línea
- `o`: insertar una nueva linea debajo de la línea actual
- `O`: insertar una nueva línea encima de la línea actual
- `ea`: insertar después de palabra
- `Ctrl + h`: eliminar el carácter antes del cursor durante el modo de inserción
- `Ctrl + w`: eliminar palabra antes del cursor durante el modo de inserción
- `Ctrl + j`: comenzar una nueva línea durante el modo de inserción
- `Ctrl + t`: sangrar (mover a la derecha) línea un ancho de cambio durante el modo de inserción
- `Ctrl + d`: desangrar (mover a la izquierda) línea un ancho de turno durante el modo de inserción
- `Ctrl + n`: insertar (autocompletar) la siguiente coincidencia antes del cursor durante el modo de inserción
- `Ctrl + p`: insertar (autocompletar) coincidencia anterior antes del cursor durante el modo de inserción
- `Ctrl + rx`: insertar el contenido del registro x
- `Esc`: salir del modo insertar

## Editar

- `r`: reemplazar un carácter
- `J`: juntar siguiente línea con la actual
- `gJ`: une la línea de abajo a la actual sin espacio entre ellas
- `gwip`: párrafo de reflujo
- `g~`: cambiar caso a movimiento
- `gu`: cambiar a minúsculas hasta movimiento
- `gU`: cambiar a mayúsculas hasta el movimiento
- `cc`: cambiar (reemplazar) toda la línea
- `C`: cambiar (reemplazar) al final de la línea
- `c$`: cambiar (reemplazar) hasta el final de la línea
- `ciw`: cambiar (reemplazar) palabra completa
- `cw`: cambiar (reemplazar) una palabra
- `s`: eliminar carácter y reemplazar texto
- `S`: eliminar línea y reemplazar texto (igual que el comando cc)
- `xp`: transponer dos letras (suprimir y pegar)
- `u`: deshacer
- `U`: restaurar (deshacer) la última línea modificada
- `Ctrl + r`: rehacer
- `.`: repetir el último comando

## Modo visual - seleccionar texto

- `v`: iniciar modo visual, seleccionar lineas y ejecutar un comando (como y-yank)
- `2V`: iniciar modo visual seleccionando toda la línea
- `o`: moverse al otro extremo de la zona seleccionada
- `Ctrl + v`: iniciar modo visual de bloque (visual block mode)
- `O`: moverse a la otra esquina del bloque
- `aw`: seleccionar una palabra
- `ab`: un bloque con ()
- `aB`: un bloque con {}
- `at`: un bloque con etiquetas <>
- `ib`: un bloque interno con ()
- `iB`: un bloque interno con {}
- `it`: bloque interior con etiquetas <>
- `Esc`: salir del modo visual

> **Tip**: Instead of b or B one can also use ( or { respectively.

## Comandos visuales

- `>`: mover texto hacia la derecha
- `<`: mover texto hacia la izquierda
- `y`: copiar texto seleccionado
- `d`: eliminar texto seleccionado
- `~`: pasar de minúscula a mayúscula y viceversa
- `u`: cambiar el texto marcado a minúsculas
- `U`: cambiar el texto marcado a mayúsculas

## Registros

- `:reg[isters]`: mostrar el contenido de los registros
- `"xy`: copiar en el registro X
- `"xp`: pegar el contenido del registro X
- `"+y`: copiar en el registro del portapapeles del sistema
- `"+p`: pegar desde el registro del portapapeles del sistema

> Los registros son guardados en ~/.viminfo, y serán cargados la próxima vez que se ejecute vim.

### Registros especiales

- `0`: ultima copia
- `"`: registro sin nombre, última eliminación o copia
- `%`: nombre de archivo actual
- `#`: nombre de archivo alternativo
- `*`: contenido del portapapeles (X11 primario)
- `+`: contenido del portapapeles (portapapeles X11)
- `/`: último patrón de búsqueda
- `:`: última línea de comandos
- `.`: último texto insertado
- `-`: último pequeño (menos de una línea) eliminar
- `=`: registro de expresión
- `_`: registro de agujero negro

## Marcas

- `:marks`: mostrar marcas
- `ma`: definir posición actual para la marca A
- ``` `a```: saltar a la posición de la marca A
- ``` y`a ```: copiar texto a la posición de la marca A
- ``` `0```: ir a la posición donde se había salido previamente de Vim
- ``` `"```: ir a la posición en la que se editó por última vez este archivo
- ``` `.```: ir a la posición del último cambio en este archivo
- ``` `` ```: ir a la posición antes del último salto
- `:ju[mps]`: lista de saltos
- `Ctrl + i`: ir a la nueva posición en la lista de salto
- `Ctrl + o`: ir a la posición anterior en la lista de salto
- `:changes`: lista de cambios
- `g,`: ir a la nueva posición en la lista de cambios
- `g;`: ir a la posición anterior en la lista de cambios
- `Ctrl + ]`: saltar a la etiqueta debajo del cursor

> **Tip**: To jump to a mark you can either use a backtick (`) or an apostrophe ('). Using an apostrophe jumps to the beginning (first non-black) of the line holding the mark.

## Macros

- `qa`: grabar macro a
- `q`: detener la grabación de la macro
- `@a`: ejecutar macro a
- `@@`: re-ejecutar la última macro ejecutada

## Copiar y pegar

- `yy`: copiar una línea
- `2yy`: copiar dos líneas
- `yw`: copiar el resto de los carácteres de la palabra desde la posición del cursor hasta el principio de la siguiente palabra
- `y$`: copiar hasta el final de la línea
- `p`: pegar antes del cursor
- `P`: pegar después del cursor
- `dd`: cortar una línea
- `2dd`: cortar dos líneas
- `dw`: cortar el resto de los carácteres de la palabra desde la posición del cursor hasta el principio de la siguiente palabra
- `D`: cortar hasta el final de la línea
- `d$`: cortar hasta el final de la línea
- `x`: cortar carácter

## Sangria del texto

- `>>`: sangría (mover a la derecha) línea uno ancho de turno
- `<<`: desangrar (mover a la izquierda) línea uno ancho de turno
- `>%`: sangrar un bloque con () o {} (cursor sobre llave)
- `>ib`: sangrar el bloque interior con ()
- `>at`: sangrar un bloque con etiquetas <>
- `3==`: re-sangrar 3 líneas
- `=%`: volver a sangrar un bloque con () o {} (cursor sobre llave)
- `4=iB`: volver a sangrar el bloque interior con {}
- `gg=G`: volver a sangrar todo el búfer
- `]p`: pegar y ajustar la sangría a la línea actual

## Salir

- `:w`: guardar archivo sin salir
- `:w !sudo tee %`: guardar archivo usando sudo
- `:wq` or `:x` or `ZZ`: guardar archivo y salir
- `:q`: salir (no funciona si hay cambios sin guardar)
- `:q!` or `ZQ`: salir descartando los cambios no guardados
- `:wqa`: escribir (guardar) y salir en todas las pestañas

## Buscar y reemplazar

- `/pattern`: buscar patrón
- `?pattern`: buscar patrón hacia atrás
- `\vpattern`: patrón 'muy mágico' (very magic): los caracteres no alfanuméricos son interpretados como caracteres especiales regex (sin necesidad de escaparlos)
- `n`: repetir la búsqueda en la misma dirección
- `N`: repetir la búsqueda en la dirección contraria
- `:%s/old/new/g`: reemplazar en todo el archivo
- `:%s/old/new/gc`: reemplazar en todo el archivo pidiendo una confirmación
- `:noh[lsearch]`: desactiva el realce (highlight) de los resultados de búsqueda

## Buscar en varios archivos

- `:vim[grep] /pattern/ {file}`: buscar un patrón en varios archivos
- `:cn[ext]`: ir al resultado siguiente
- `:cp[revious]`: ir al resultado previo
- `:cope[n]`: abrir ventana con una lista de los resultados
- `:ccl[ose]`: cerrar la ventana de corrección rápida

## Pestañas

- `:tabnew or :tabnew {page.words.file}`: abrir archivo en una nueva pestaña
- `Ctrl + wT`: mover la ventana actual a su propia pestaña
- `gt or :tabn[ext]`: irr a la próxima pestaña
- `gT or :tabp[revious]`: ir a la pestaña anterior
- `#gt`: ir a la pestaña número #
- `:tabm[ove] #`: mover a la pestaña actual a la posición número # (desde 0)
- `:tabc[lose]`: cerrar todas las pestañas y sus ventanas
- `:tabo[nly]`: cerrar todas las pestañas salvo la actual
- `:tabdo command`: run the command on all tabs (e.g. :tabdo q - closes all opened tabs)
