---
date: 2023-10-21T15:30
title: Braille y Morse
author: AlbertoVf
tags: ['código', 'lenguaje', 'no verbal']
resume: código braille y morse
---

# Código Braille y Morse

## Código Braille

![alfabeto](braille.jpg)
> alfabeto braille y números

## Código Morse

![International_Morse_code](International_Morse_code.jpg)
