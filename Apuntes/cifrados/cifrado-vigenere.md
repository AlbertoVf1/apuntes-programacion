---
date: 2023-10-21T15:43
title: Cifrado Vigenere
author: AlbertoVf
tags: ['código', 'encriptacion']
resume: cifrado vigenere
---

# Cifrado Vigenere

El cifrado Vigenere es un criptoanalisis simétrico, es decir, utiliza la misma clave para cifrar y descifrar. El cifrado Vigenere se asemeja al cifrado cesar, pero su diferencia radica en que el primero utiliza una clave mas larga para contrarrestar el problema del cifrado cesar. Para resolver este problema se utiliza una palabra clave en vez de una letra.

Se codifica el texto agregando una palabra `palabra clave` a cada una de sus letras. La palabra clave se agrega indefinidamente en el texto a cifrar, y después de agrega el código  ASCII de cada letra dando lugar a una sucesión de números.

```text
mensaje:      P A R I S  V A U T  B I E N  U N E  M E S S E
clave:        L O U P L  O U P L  O U P L  O U P  L O U P L
criptograma:  A O M X D  K U K E  P C T X  J H T  W S N I O
```

> Muestra de codificación de palabra

| LETRA | VALOR |     | LETRA | VALOR |     | LETRA | VALOR |     | LETRA | VALOR |
| ----- | ----- | --- | ----- | ----- | --- | ----- | ----- | --- | ----- | ----- |
| A     | 65    |     | a     | 97    |     | N     | 78    |     | n     | 110   |
| B     | 66    |     | b     | 98    |     | O     | 79    |     | o     | 111   |
| C     | 67    |     | c     | 99    |     | P     | 80    |     | p     | 112   |
| D     | 68    |     | d     | 100   |     | Q     | 81    |     | q     | 113   |
| E     | 69    |     | e     | 101   |     | R     | 82    |     | r     | 114   |
| F     | 70    |     | f     | 102   |     | S     | 83    |     | s     | 115   |
| G     | 71    |     | g     | 103   |     | T     | 84    |     | t     | 116   |
| H     | 72    |     | h     | 104   |     | U     | 85    |     | u     | 117   |
| I     | 73    |     | i     | 105   |     | V     | 86    |     | v     | 118   |
| J     | 74    |     | j     | 106   |     | W     | 87    |     | w     | 119   |
| K     | 75    |     | k     | 107   |     | X     | 88    |     | x     | 120   |
| L     | 76    |     | l     | 108   |     | Y     | 89    |     | y     | 121   |
| M     | 77    |     | m     | 109   |     | Z     | 90    |     | z     | 122   |

![Cuadro Vigenere](./Cuadro_Vigenere.jpg)
> Cuadro con las representaciones de las letras
