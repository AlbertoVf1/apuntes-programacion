---
date: 2023-06-03T17:00
title: Apuntes Git
author: AlbertoVf
tags: ['documentación', 'git', 'flujo de trabajo', 'comandos']
resume: Apuntes sobre el sistema de control de versiones git.
---

# Apuntes Git

## Que es Git

**Git**[^1] es un *software* de [control de versiones](https://es.wikipedia.org/wiki/Control_de_versiones "Control de versiones") diseñado por [Linus Torvalds](https://es.wikipedia.org/wiki/Linus_Torvalds "Linus Torvalds"), pensando en la eficiencia, la confiabilidad y compatibilidad del mantenimiento de versiones de aplicaciones cuando estas tienen un gran número de archivos de [código fuente](https://es.wikipedia.org/wiki/C%C3%B3digo_fuente). Su propósito es llevar registro de los cambios en archivos de computadora incluyendo coordinar el trabajo que varias personas realizan sobre archivos compartidos en un repositorio de código.

Al principio, Git se pensó como un motor de bajo nivel sobre el cual otros pudieran escribir la interfaz de usuario o [front end](https://es.wikipedia.org/wiki/Front-end_y_back-end) como [Cogito](https://es.wikipedia.org/wiki/Cogito_(software)) o StGIT.​ Sin embargo, Git se ha convertido desde entonces en un sistema de control de versiones con funcionalidad plena.​ Hay algunos proyectos de mucha relevancia que ya usan Git, en particular, el grupo de [programación](https://es.wikipedia.org/wiki/Programaci%C3%B3n) del [núcleo Linux](https://es.wikipedia.org/wiki/N%C3%BAcleo_Linux).

El [mantenimiento del software](https://es.wikipedia.org/wiki/Mantenimiento_de_Software) Git está actualmente (2009) supervisado por Junio Hamano, quien recibe contribuciones al código de alrededor de 280 programadores. En cuanto a derechos de autor Git es un [software libre](https://es.wikipedia.org/wiki/Software_libre) distribuible bajo los términos de la versión 2 de la [Licencia Pública General de GNU](https://es.wikipedia.org/wiki/GNU_General_Public_License).

### Configurar git

La configuración[^2] de git se guardara en el fichero `.git/config` desde la raíz del repositorio. Para guardar la configuración a nivel global se agregara el tag `--global` en los script, guardándose en el archivo `.gitconfig` en la carpeta del usuario.

En el fichero se guardaran desde el usuario y correo que se utiliza para la información de commits hasta los editores que utiliza git y la configuración de los repositorios.

```bash
## configurar nombre de usuario
git config --global user.name ""
## configurar email del usuario
git config --global user.email ""
```

```bash
## configurar el editor para git
git config --global core.editor ""
## configurar fin de linea de los ficheros
git config --global core.eol "lf|crlf"
## comprueba los permisos al buscar cambios
git config --global core.filemode "false|true"
## comprueba los espacios|tabs al buscar cambios
git config --global core.whitespace "false|true"
## configura los enlaces
git config --global core.symlinks "false|true"
```

```bash
## crea un alias para "add" se ejecuta como "git a"
git config --global alias.a add
## alias para "commit -m"
git config --global alias.c commit -m
## alias para "git difftool"
git config --global alias.d difftool
```

## Comandos básicos

- `git init`: Esto crea un subdirectorio nuevo llamado .git, el cual contiene todos los archivos necesarios del repositorio – un esqueleto de un repositorio de Git. Todavía no hay nada en tu proyecto que esté bajo seguimiento.
- `git fetch`: Descarga los cambios realizados en el repositorio remoto.
- `git merge <nombre_rama>`: Impacta en la rama en la que te encuentras parado, los cambios realizados en la rama "nombre_rama".
- `git pull`: Unifica los comandos *fetch* y *merge* en un único comando.
- `git commit -m "<mensaje>"`: Confirma los cambios realizados. El "mensaje" generalmente se usa para asociar al *commit* una breve descripción de los cambios realizados.
- `git push origin <nombre_rama>`: Sube la rama "nombre_rama" al servidor remoto.
- `git status`: Muestra el estado actual de la rama, como los cambios que hay sin commitear.
- `git add <nombre_archivo>`: Comienza a trackear el archivo "nombre_archivo".
- `git checkout -b <nombre_rama_nueva>`: Crea una rama a partir de la que te encuentres parado con el nombre "nombre_rama_nueva", y luego salta sobre la rama nueva, por lo que quedas parado en esta última.
- `git checkout -t origin/<nombre_rama>`: Si existe una rama remota de nombre "nombre_rama", al ejecutar este comando se crea una rama local con el nombre "nombre_rama" para hacer un seguimiento de la rama remota con el mismo nombre.
- `git branch`: Lista todas las ramas locales.
- `git branch -a`: Lista todas las ramas locales y remotas.
- `git branch -d <nombre_rama>`: Elimina la rama local con el nombre "nombre_rama".
- `git push origin <nombre_rama>`: Commitea los cambios desde el branch local origin al branch "nombre_rama".
- `git remote prune origin`: Actualiza tu repositorio remoto en caso de que algún otro desarrollador haya eliminado alguna rama remota.
- `git reset --hard HEAD`: Elimina los cambios realizados que aún no se hayan hecho *commit*.
- `git revert <hash_commit>`: Revierte el *commit* realizado, identificado por el "hash_commit".

## Commits

Un commit, en el contexto de la programación y el control de versiones, se refiere a la acción de guardar los cambios realizados en un repositorio de código fuente. Un repositorio es una ubicación centralizada donde se almacenan y gestionan los archivos de un proyecto.

Cuando trabajas en un proyecto de desarrollo de software, es común realizar cambios en los archivos para agregar nuevas características, corregir errores o mejorar el código existente. Cada vez que deseas guardar esos cambios, creas un commit.

Un commit representa un conjunto lógico de modificaciones relacionadas entre sí. Por lo general, se compone de uno o más archivos modificados, agregados o eliminados en el repositorio. Cada commit se identifica mediante un identificador único, generalmente un número o un hash.

Al crear un commit, se guarda una instantánea del estado actual de los archivos del proyecto en ese momento específico. Esta instantánea incluye información sobre los cambios realizados, el autor del commit y un mensaje descriptivo que resume los cambios efectuados.

Los commits son fundamentales para el control de versiones, ya que permiten rastrear y gestionar el historial de cambios en un proyecto a lo largo del tiempo. Al utilizar un sistema de control de versiones, como Git, puedes navegar por el historial de commits, comparar versiones anteriores, revertir cambios o fusionar ramas de desarrollo.

En resumen, un commit es una forma de guardar y etiquetar los cambios realizados en un repositorio de código fuente. Es una práctica común y esencial en el desarrollo de software para mantener un historial de versiones y colaborar de manera efectiva en proyectos.

![ciclo de vida](ciclo-de-vida-en-git.jpg)

### Conventional Commit

La especificación de Commits Convencionales[^3] es una convención ligera sobre los mensajes de commits. Proporciona un conjunto sencillo de reglas para crear un historial de commits explícito; lo que hace más fácil escribir herramientas automatizadas encima del historial. Esta convención encaja con [SemVer](http://semver.org/lang/es/), al describir en los mensajes de los commits las funcionalidades, arreglos, y cambios de ruptura hechos.

```git
<tipo>[ámbito opcional]: <descripción>

[cuerpo opcional]

[nota(s) al pie opcional(es)]
```

### Tipos de commits

```bash
## iniciar proyecto
"🎉 init(build): Initial commit"
## agregar una nueva característica
"✨ feat: "
## solucionar errores
"🐞 fix: "
## agregar documentación
"📃 docs: "
## formatear código
"🌈 style: "
## refactor de código (mover ficheros, refactoring...)
"🦄 refactor: "
## optimización
"🎈 perf: "
## escribir test
"🧪 test: "
## relacionado con dependencias
"🔧 build: "
## integración continua
"🐎 ci: "
## realizar tarea (publicar version, merge ...)
"🐳 chore: "
## revertir un commit
"↩ revert: "
```

### Editar commits

```bash
git commit -m 'msg' ## Crea un commit con un mensaje
```

```bash
## Agrega los cambios al commit anterior permitiendo editar el mensaje
git commit --amend
## Agrega los cambios al co,mit anterior manteniendo el mensaje
git commit --amend --no-edit
```

```bash
## Permite editar los commits desde HEAD a X
## Se abre el editor de git que tengas configurado y
## editaras cada mensaje individualmente
git rebase -i HEAD~X
```

![Ejemplo de git rebase](git-rebase-todo.jpg)

## Flujos de trabajo

Git plantea una gran libertad en la forma de trabajar en torno a un proyecto. Sin embargo, para coordinar el trabajo de un grupo de personas en torno a un proyecto es necesario acordar como se va a trabajar con Git. A estos acuerdos se les llama flujo de trabajo.[^4] Un flujo de trabajo de Git es una fórmula o una recomendación acerca del uso de Git para realizar trabajo de forma uniforme y productiva.[^5] Los flujos de trabajo más populares son git-flow, GitHub-flow, GitLab Flow y One Flow.[^6]

### Git-Flow

Creado en 2010 por Vincent Driessen. Es el flujo de trabajo más conocido. Está pensado para aquellos proyectos que tienen entregables y ciclos de desarrollo bien definidos.[^5] Está basado en dos grandes ramas con infinito tiempo de vida (ramas master y develop) y varias ramas de apoyo, unas orientadas al desarrollo de nuevas funcionalidades (ramas feature-\*), otras al arreglo de errores (hotfix-\*) y otras orientadas a la preparación de nuevas versiones de producción (ramas release-\*). La herramienta gitflow[^7] facilita la automatización de las tareas implicadas en este flujo de trabajo.

- `Master`: Es la rama principal. Contiene el repositorio que se encuentra publicado en producción, por lo que debe estar siempre estable.
- `Development`: Es una rama sacada de Master. Es la rama de integración, todas las nuevas funcionalidades se deben integrar en esta rama. Luego que se realice la integración y se corrijan los errores (en caso de haber alguno), es decir que la rama se encuentre estable, se puede hacer un merge de development sobre la rama Master.
- `Features`: Cada nueva funcionalidad se debe realizar en una rama nueva, específica para esa funcionalidad. Estas se deben sacar de Development. Una vez que la funcionalidad esté desarrollada, se hace un merge de la rama sobre Development, donde se integrará con las demás funcionalidades.
- `Hotfix`: Son errores de software que surgen en producción, por lo que se deben arreglar y publicar de forma urgente. Es por ello, que son ramas sacadas de Master. Una vez corregido el error, se debe hacer una unificación de la rama sobre Master. Al final, para que no quede desactualizada, se debe realizar la unificación de Master sobre Development.
- `Release`: Las ramas de release apoyan la preparación de nuevas versiones de producción. Para ellos se arreglan muchos errores menores y se preparan adecuadamente los metadatos. Se suelen originar de la rama develop y deben fusionarse en las ramas master y develop.

![git-flow](git-flow.jpg)

### GitHub Flow

Creado en 2011 por GitHub[^4] y es la forma de trabajo sugerida por las funcionalidades propias de GitHub. Está centrado en un modelo de desarrollo iterativo y de despliegue constante. Está basado en cinco principios:[^5]

- Todo lo que está en la rama master está listo para ser puesto en producción
- Para trabajar en algo nuevo, debes crear una nueva rama a partir de la rama master con un nombre descriptivo. El trabajo se irá integrando sobre esa rama en local y regularmente también a esa rama en el servidor
- Cuando se necesite ayuda o información o cuando creemos que la rama está lista para integrarla en la rama master, se debe abrir una pull request (solicitud de integración de cambios).
- Alguien debe revisar y visar los cambios para fusionarlos con la rama master
- Los cambios integrados se pueden poner en producción.
- GitHub intenta simplificar la gestión de ramas, trabajando directamente sobre la rama master y generando integrando las distintas features directamente a esta rama[^8]

![github-flow](github-flow.jpg)

### GitLab Flow

Creado en 2014 por **Gitlab**.[^4]​Es una especie de extensión de GitHub Flow acompañado de un conjunto de pautas y mejores prácticas que apuntan a estandarizar aún más el proceso. Al igual que GitHub Flow propone el uso de ramas de funcionalidad (feature) que se originan a partir de la rama master y que al finalizarse se mezclan con la rama master. Además introduce otros dos tipos de ramas:[^9]

- **Ramas de entorno**. Por ejemplo **pre-production, production**. Se crean a partir de la rama master cuando estamos listos para implementar nuestra aplicación. Si hay un error crítico lo podemos arreglar en un rama y luego fusionar en la rama de entorno.
- **Ramas de versión**. Por ejemplo **1.5-stable, 1.6-stable**. El flujo puede incluir ramas de versión en caso de que el software requiera lanzamientos frecuentes.

![gitlab-flow](gitlab-flow.jpg)

### One Flow

Creado en 2015 por Adam Ruka. En él cada nueva versión de producción está basada en la versión previa de producción. La mayor diferencia entre One Flow y Git Flow es que One Flow no tiene rama de desarrollo.[^7]

![one-flow](one-flow.jpg)

[^1]: <https://es.wikipedia.org/wiki/Git>

[^2]: <https://git-scm.com/docs/git-config>

[^3]: <https://www.conventionalcommits.org/es/v1.0.0/>

[^4]: <https://web.archive.org/web/20130824050831/https://www.yukei.net/2013/08/flujos-de-trabajo-en-git>

[^5]: <https://www.atlassian.com/es/git/tutorials/comparing-workflows>

[^6]: <https://medium.com/@patrickporto/4-branching-workflows-for-git-30d0aaee7bf>

[^7]: <https://github.com/nvie/gitflow>

[^8]: <https://enmilocalfunciona.io/git-como-gestionar-y-cuidar-nuestro-codigo/>

[^9]: <http://git.dokry.com/cul-es-la-diferencia-entre-github-flow-y-gitlab-flow.html>
