---
date: 2023-06-03T21:03
title: Apuntes markdown
author: AlbertoVf
tags: ['markdown', 'documentacion']
resume: Apuntes sobre que es markdown y como se utiliza
---

# Apuntes Markdown

## Que es Markdown?

**Markdown**[^1] es un [lenguaje de marcado ligero](https://es.wikipedia.org/wiki/Lenguajes_de_marcas_ligeros "Lenguajes de marcas ligeros") creado por [John Gruber](https://es.wikipedia.org/wiki/John_Gruber) y [Aaron Swartz](https://es.wikipedia.org/wiki/Aaron_Swartz) que trata de conseguir la máxima legibilidad y facilidad de publicación tanto en su forma de entrada como de salida, inspirándose en muchas convenciones existentes para marcar mensajes de correo electrónico usando texto plano. Se distribuye bajo [licencia BSD](https://es.wikipedia.org/wiki/Licencia_BSD) y se distribuye como plugin (o al menos está disponible) en diferentes [sistemas de gestión de contenidos (CMS)](https://es.wikipedia.org/wiki/Sistema_de_gesti%C3%B3n_de_contenidos).
Markdown convierte el texto marcado en documentos [XHTML](https://es.wikipedia.org/wiki/XHTML) utilizando html2text creado por [Aaron Swartz](https://es.wikipedia.org/wiki/Aaron_Swartz).

Markdown fue implementado originariamente en [Perl](https://es.wikipedia.org/wiki/Perl) por Gruber, pero desde entonces ha sido traducido a multitud de [lenguajes de programación](https://es.wikipedia.org/wiki/Lenguaje_de_programaci%C3%B3n),
incluyendo [PHP](https://es.wikipedia.org/wiki/PHP), [Python](https://es.wikipedia.org/wiki/Python), [Ruby](https://es.wikipedia.org/wiki/Ruby), [Java](https://es.wikipedia.org/wiki/Lenguaje_de_programaci%C3%B3n_Java) y [Common Lisp](https://es.wikipedia.org/wiki/Common_Lisp).

> extensiones de fichero: .md, .markdown

## Elementos de bloque

### Párrafos y saltos de linea

Para generar un nuevo párrafo en Markdown simplemente separa el texto mediante una línea en blanco (pulsando dos veces intro). Al igual que sucede con HTML, Markdown no soporta dobles líneas en blanco, así que si intentas generarlas estas se convertirán en una sola al procesarse.

Para realizar un salto de línea y empezar una frase en una línea siguiente dentro del mismo párrafo, tendrás que pulsar dos veces la barra espaciada antes de pulsar una vez intro.
Por ejemplo si quisieses escribir un poema Haiku quedaría tal que así:

"Andando con sus patitas mojadas,

el gorrión

por la terraza de madera"

Donde cada verso tiene dos espacios en blanco al final.

### Encabezados

Las `# almohadillas` son uno de los métodos utilizados en Markdown para crear encabezados. Debes usarlos añadiendo uno por cada nivel.

Representan secciones del documento. Para su escritura se utiliza el carácter `#` de 1 a 6 veces dependiendo del nivel de la sección.

```markdown
# Encabezado 1
## Encabezado 2
### Encabezado 3
#### Encabezado 4
##### Encabezado 5
###### Encabezado 6
```

También puedes cerrar los encabezados con el mismo número de almohadillas, por ejemplo `### Encabezado 3 ###`.

Existe otra manera de generar encabezados, aunque este método está limitado a dos niveles.

Consiste en subrayar los encabezados con el símbolo `=` para el encabezado 1, o con guiones `-` para el encabezado 2.

Es decir: Esto sería un encabezado 1 `===`. Esto sería un encabezado 2 `—-`. No existe un número concreto `=` o `-` que necesites escribir para que esto funcione, ¡incluso bastaría con uno

### Citas

Las citas se generar utilizando el carácter *mayor que* `>` al comienzo del bloque de texto.

```markdown
> Un país, una civilización se puede juzgar por la forma en que trata a sus animales.
> — Mahatma Gandhi
```

Si la cita en cuestión se compone de **varios párrafos**, deberás añadir el mismo símbolo `>` al comienzo de cada uno de ellos.

```markdown
> Creo que los animales ven en el hombre un ser igual a ellos que ha perdido de forma extraordinariamente peligrosa el sano intelecto animal.
>
> Es decir, que ven en él al animal irracional, al animal que ríe, al animal que llora, al animal infeliz. — Friedrich Nietzsche
```

Incluso puedes concatenar varios `>>` para crear **citas anidadas**.

```markdown
> Esto sería una cita como la que acabas de ver.
>
> > Dentro de ella puedes anidar otra cita.
>
> La cita principal llegaría hasta aquí.
```

Recuerda separar los saltos de línea con `>`, o `>>` si te encuentras dentro de la cita anidada; para crear párrafos dentro del mismo bloque de cita.

### Listas

Para crear **listas desordenadas** utiliza `*` asteriscos, `-` guiones, o `+` símbolo de suma.

Para crear listas ordenadas debes utilizar la sintaxis de tipo: **número.**. Al igual que ocurre con las listas desordenadas, también podrás anidarlas o combinarlas.

```markdown
-   lista desordenada
    -   elemento 1
    -   elemento 2
-   lista desordenada

1. lista ordenada
    1. elemento 1
    2. elemento 2
2. lista ordenada

-   [ ] lista de tareas
-   [x] lista de tareas
```

### Tablas

```markdown
| Columna 1              |          Columna 2 | Columna 3            |   Columna 4    |
| ---------------------- | -----------------: | :------------------- | :------------: |
| alineación por defecto | alineación derecha | alineación izquierda | Texto centrado |
```

### Bloque de código

Si quieres crear un bloque entero que contenga código. Lo único que tienes que hacer es encerrar dicho párrafo entre dos líneas formadas por tres `~` virgulillas o tres `` ` `` acentos.

### Formatos de matemáticas

Sirve para crear un bloque que represente una operación matemática. Se representa con dos `$` al comienzo y final en lineas independientes.

En el bloque para representar un espacio en blanco se utiliza `\` y para realizar saltos de linea `\\`

$$
x = Operaciones\ matemáticas \ y \\y = Operaciones \\
z >= 0
$$

### Reglas horizontales

Las reglas horizontales se utilizan para separar secciones de una manera visual. Las estás viendo constantemente en este artículo ya que las estoy utilizando para separar los diferentes elementos de sintaxis de Markdown.

Para crearlas, en una línea en blanco deberás incluir tres de los siguientes elementos: asteriscos `*`, guiones `-` , o guiones bajos `_` .

## Elementos de linea

### Énfasis

Permiten resaltar palabras en un párrafo. Entre los distintos tipos de resaltado hay: negrita, cursiva, tachado o resaltar su fondo.

Para resaltar en negrita o cursiva se utiliza el carácter `*` o `_` dos veces para negrita, una vez para cursiva y tres veces para ambas a la vez.

```markdown
**Texto en negrita**
**Texto en negrita**

_Texto en cursiva_
_Texto en cursiva_

**_Texto en negrita cursiva_**
**_Texto en negrita cursiva_**

==Texto resaltado==
~~Texto tachado~~
```

### Imágenes

Insertar una imagen con Markdown se realiza de una manera prácticamente idéntica a insertar [links](#links).

Solo que en este caso, deberás añadir un símbolo de **`!` exclamación** al principio y el **enlace** no será otro que **la ubicación de la imagen**.

```markdown
![Texto alternativo](/ruta/a/la/imagen.jpg)
```

El *texto alternativo* es lo que se mostraría si la carga de la imagen fallase.

También podrás añadir un **título alternativo** entrecomillándose al final de la ruta. Esto sería el título mostrado al dejar el cursor del ratón sobre la imagen.

```markdown
![Texto alternativo](/ruta/a/la/imagen.jpg "Título alternativo")
```

Ya que al añadir imágenes también estás tratando con URLs, puedes utilizar el método que viste anteriormente para **incluir links mediante referencias**, solo que en este caso **los enlaces de referencia serán aquellos donde se encuentre tu imagen**.

```markdown
De esta forma podrías insertar una imagen
![nombre de la imagen][img1]
O dos, sin ensuciar tu espacio de escritura.
![nombre de la imagen2][img2]
[img1]: /ruta/a/la/imagen.jpg "Título alternativo"
[img2]: /ruta/a/la/imagen2.jpg "Título alternativo"
```

### Links o enlaces

Añadir enlaces a una publicación, más que común, hoy en día es algo casi obligatorio. Con Markdown tendrás varias formas de hacerlo.

#### Links o enlaces en línea

Son los enlaces de toda la vida. Como su nombre indica, se encuentran en línea con el texto.

Se crean escribiendo la palabra o texto enlazada entre [] corchetes, y el link en cuestión entre () paréntesis.

```markdown
[enlace en línea](https://markdown.es/)
```

#### Links o enlaces como referencia

La desventaja del método anterior, es que si utilizas links demasiado complejos o largos pueden dificultarte la lectura de tu texto.
Para solucionarlo y crear tu contenido de una manera más ordenada puedes generar enlaces de referencia.
Esto quiere decir que en tu texto enlazarás palabras o códigos concretos (formados por letras y/o números), que en otro lugar más apartado de tu documento tendrás definidos como determinadas URL.

```markdown
[nombre que quieres darle a tu enlace][nombre de tu referencia]

[nombre de tu referencia]: [https://www.markdownguide.org/basic-syntax/]
```

<!-- ## Enlaces de interés-->
<!-- <https://markdown.es/> -->
<!-- <https://www.markdownguide.org/basic-syntax/> -->

[^1]: <https://es.wikipedia.org/wiki/Markdown>
