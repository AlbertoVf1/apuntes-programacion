---
date: 2023-08-16T12:18
title: visual studio code
author: AlbertoVf
tags: ['editores', 'vscode', 'configuracion']
resume: Que es vscode y configuracion basica
---

# Visual Studio Code

## Que es Visual Studio Code ?

Visual Studio Code[^1] es un editor de código fuente ligero pero potente que se ejecuta en su escritorio y está disponible para Windows, macOS y Linux.Viene con soporte integrado para JavaScript, TypeScript y Node.js y tiene un rico ecosistema de extensiones para otros lenguajes y tiempos de ejecución(como C, C#, Java, Python, PHP, Go, .NET).Comience su viaje con VS Code con estos videos introductorios.

## Configuración de fuentes y tamaño de linea

Configuración del editor para seleccionar una fuente, tamaño de texto y linea, espaciado entre lineas y caracteres, comportamiento de los espacios y guías del texto.

```json
"editor.fontFamily": "CaskaydiaCove Nerd Font, monospace",
"editor.fontLigatures": true,
"editor.fontVariations": true,
"editor.guides.bracketPairs": false,
"editor.guides.bracketPairsHorizontal": false,
"editor.insertSpaces": false,
"editor.letterSpacing": 1,
"editor.lineHeight": 2.1,
"editor.renderWhitespace": "all",
"editor.wordWrapColumn": 120,
```

[^1]: <https://code.visualstudio.com/>
