---
date: 2024-03-03T23:34
title: conversion formatos
author: AlbertoVf
tags: ['convert','csv','json','yaml']
resume: Conversion entre distintos formatos de archivos
---

# Conversion de archivos

## Desde Csv a JSON/YAML

```python
def read_from_csv():
    csv_file_path = path + ".csv"
    data_csv = []
    with open(csv_file_path, "r") as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=";")
        for row in csv_reader:
            data_csv.append(row)
    return data_csv
```

> lectura de un fichero csv con encabezado y campos separadso por ';'

```python
import yaml, json
def export_to_yaml():
    yaml_file_path = path + ".yml"
    data = yaml.safe_load(open(yaml_file_path, "r"))
    data["software"] = read_from_csv()
    yaml.dump(data, open(yaml_file_path, "w"), default_flow_style=False)

def export_to_json():
    json_file_path = path + ".json"
    data = json.load(open(json_file_path, "r"))
    data["software"] = read_from_csv()
    json.dump(data, open(json_file_path, "w"))
```

> Guarda los datos leidos del csv en la propiedad 'software' del fichero yaml
