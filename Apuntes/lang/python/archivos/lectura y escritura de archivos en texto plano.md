---
date: 2024-07-10T12:50
title: lectura y escritura de archivos en texto plano
author: AlbertoVf
tags: ['txt','csv', 'binario']
resume: Como leer y escribir archivos de texto plano y binarios
---

# Lectura y escritura de archivos

## Archivos de Texto

### Lectura de Texto

```python
# Abrir un archivo para lectura
with open('archivo.txt', 'r') as file:
    contenido = file.read()
    print(contenido)
```

### Escritura de Texto

```python
# Abrir un archivo para escritura
with open('nuevo_archivo.txt', 'w') as file:
    file.write("Hola, este es un nuevo archivo.\n")
    file.write("¡Espero que estés disfrutando de la lectura!")
```

## Archivos CSV

### Lectura de Archivos CSV

```python
import csv

# Abrir un archivo CSV para lectura
with open('datos.csv', 'r', newline='') as file:
    lector_csv = csv.reader(file)

    for fila in lector_csv:
        print(fila)
```

### Escritura de CSV

```python
import csv

# Datos a escribir en el archivo CSV
datos = [
    ['Nombre', 'Edad', 'Ciudad'],
    ['Juan', 25, 'Madrid'],
    ['María', 30, 'Barcelona']
]

# Abrir un archivo CSV para escritura
with open('nuevos_datos.csv', 'w', newline='') as file:
    escritor_csv = csv.writer(file)

    for fila in datos:
        escritor_csv.writerow(fila)
```

## Binarios

### Lectura de Binarios

```python
# Abrir un archivo binario para lectura
with open('imagen.jpg', 'rb') as file:
    datos_binarios = file.read()
    # Procesar los datos binarios según sea necesario
```

### Escritura de Binarios

```python
# Abrir un archivo binario para escritura
with open('nueva_imagen.jpg', 'wb') as file:
    # Datos binarios a escribir en el archivo
    datos_binarios = obtener_datos_binarios()
    file.write(datos_binarios)
```
