---
date: 2024-07-10T13:35
title: lectura escritura json
author: AlbertoVf
tags: ['dict','json','yaml']
resume: Lectura y escritura de dict a partir de un fichero
---

# Lectura y escritura de `dict` en un fichero

Aquí te proporcionaré un documento detallado con ejemplos sobre la lectura y escritura de archivos JSON y YAML en Python, utilizando las bibliotecas estándar `json` y `PyYAML` respectivamente.

Antes de comenzar, instala la biblioteca PyYAML:

```bash
pip install PyYAML
```

## Lectura de Archivos

```python
import json

# Abrir un archivo JSON para lectura
with open('datos.json', 'r') as file:
    # Cargar datos desde el archivo JSON
    datos = json.load(file)

# Trabajar con los datos
print("Datos cargados desde el archivo JSON:")
print(datos)
```

```python
import yaml

# Abrir un archivo YAML para lectura
with open('datos.yaml', 'r') as file:
    # Cargar datos desde el archivo YAML
    datos = yaml.safe_load(file)

# Trabajar con los datos
print("Datos cargados desde el archivo YAML:")
print(datos)
```

## Escritura de Archivos

```python
import json

# Datos a escribir en el archivo JSON
datos = {
    "nombre": "Juan",
    "edad": 25,
    "ciudad": "Madrid"
}

# Abrir un archivo JSON para escritura
with open('nuevos_datos.json', 'w') as file:
    # Escribir datos en el archivo JSON
    json.dump(datos, file, indent=2)

print("Datos escritos en el archivo JSON.")
```

```python
import yaml

# Datos a escribir en el archivo YAML
datos = {
    "nombre": "María",
    "edad": 30,
    "ciudad": "Barcelona"
}

# Abrir un archivo YAML para escritura
with open('nuevos_datos.yaml', 'w') as file:
    # Escribir datos en el archivo YAML
    yaml.dump(datos, file, default_flow_style=False)

print("Datos escritos en el archivo YAML.")
```
