---
date: 2024-07-10T13:33
title: lectura y escritura de imagenes
author: AlbertoVf
tags: ['biblioteca','pìllow','rgb','image']
resume: Lectura y escritura de archivos de imagenes
---

# Lectura y Escritura de Imágenes

En Python, puedes utilizar la biblioteca Pillow para trabajar con imágenes. A continuación, se presenta un documento detallado con ejemplos de lectura y escritura de imágenes en formatos comunes como JPEG y PNG.

## Instalación de Pillow

Asegúrate de tener instalada la biblioteca Pillow antes de comenzar:

```bash
pip install Pillow
```

## Lectura de Imágenes

```python
from PIL import Image

# Ruta de la imagen a leer
ruta_imagen = 'imagen.jpg'

# Abrir la imagen
imagen = Image.open(ruta_imagen)

# Mostrar información sobre la imagen
print(f"Formato: {imagen.format}")
print(f"Modo: {imagen.mode}")
print(f"Tamaño: {imagen.size}")

# Mostrar la imagen
imagen.show()
```

## Escritura de Imágenes

```python
from PIL import Image

# Crear una nueva imagen en blanco
nueva_imagen = Image.new('RGB', (300, 200), color='white')

# Guardar la nueva imagen
nueva_ruta = 'nueva_imagen.jpg'
nueva_imagen.save(nueva_ruta)
print(f"Imagen guardada en: {nueva_ruta}")
```

## Conversión de Formato y Redimensionamiento

```python
from PIL import Image

# Ruta de la imagen a convertir
ruta_original = 'imagen.jpg'

# Abrir la imagen
imagen = Image.open(ruta_original)

# Convertir la imagen a otro formato (por ejemplo, PNG)
imagen.convert('RGB').save('imagen_convertida.png')

# Redimensionar la imagen
imagen_redimensionada = imagen.resize((200, 150))
imagen_redimensionada.save('imagen_redimensionada.jpg')
```

## Manipulación de Píxeles

```python
from PIL import Image

# Ruta de la imagen a manipular
ruta_imagen = 'imagen.jpg'

# Abrir la imagen
imagen = Image.open(ruta_imagen)

# Obtener los píxeles de la imagen
pixeles = imagen.load()

# Obtener el color del píxel en la posición (x, y)
color_pixel = pixeles[50, 50]
print(f"Color del píxel en (50, 50): {color_pixel}")

# Modificar el color de un píxel
pixeles[50, 50] = (255, 0, 0)  # Rojo
imagen.save('imagen_modificada.jpg')
```
