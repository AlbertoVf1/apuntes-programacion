---
date: 2024-07-10T17:08
title: lectura escritura pdf
author: AlbertoVf
tags: ['archivos','pdf','lectura/escritura','binario']
resume: Lectura y escritura de archivos PDF
---

# Lectura  y escritura de PDF

Aquí te proporciono un documento detallado con ejemplos sobre la lectura y escritura de archivos PDF en Python utilizando la biblioteca PyPDF2. Ten en cuenta que existen otras bibliotecas para manejar archivos PDF en Python, como `PyMuPDF` o `reportlab`, pero en este ejemplo utilizaremos PyPDF2.

```bash
pip install PyPDF2
```

> Instalación de la Biblioteca

## Lectura de Archivos PDF

```python
import PyPDF2

# Abrir un archivo PDF para lectura
with open('documento.pdf', 'rb') as file:
    # Crear un objeto de lectura de PDF
    lector_pdf = PyPDF2.PdfFileReader(file)

    # Obtener el número de páginas
    num_paginas = lector_pdf.numPages
    print(f"El documento tiene {num_paginas} páginas.\n")

    # Leer el contenido de cada página
    for pagina_num in range(num_paginas):
        # Obtener la página actual
        pagina = lector_pdf.getPage(pagina_num)

        # Extraer el texto de la página
        texto = pagina.extractText()
        print(f"Página {pagina_num + 1}:\n{texto}\n")
```

## Escritura de Archivos PDF

```python
import PyPDF2

# Crear un nuevo archivo PDF
with open('nuevo_documento.pdf', 'wb') as file:
    # Crear un objeto de escritura de PDF
    escritor_pdf = PyPDF2.PdfFileWriter()

    # Agregar una nueva página al PDF
    nueva_pagina = escritor_pdf.addPage()

    # Escribir contenido en la nueva página (opcional)
    nueva_pagina.mergePage(PyPDF2.PdfFileReader('otro_documento.pdf').getPage(0))

    # Guardar el nuevo documento
    escritor_pdf.write(file)

print("Se ha creado un nuevo documento PDF.")
```
