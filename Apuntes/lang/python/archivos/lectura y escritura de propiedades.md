---
date: 2024-07-10T12:51
title: lectura y escritura de propiedades
author: AlbertoVf
tags: ['.env','configuracion']
resume: Lectura y escritura de ficheros .env
---

# Ficheros .env

El manejo de variables de entorno en Python se puede hacer utilizando la biblioteca `python-dotenv` para cargar variables desde un archivo `.env`. Aquí tienes un documento detallado con ejemplos:

## Instalación de la Biblioteca python-dotenv

Antes de comenzar, instala la biblioteca `python-dotenv`:

```bash
pip install python-dotenv
```

## Lectura de Variables de Entorno desde un Archivo .env

Crea un archivo llamado `.env` en el directorio de tu proyecto con el siguiente contenido:

```dotenv
# Ejemplo de archivo .env
DB_HOST=localhost
DB_USER=admin
DB_PASSWORD=secreto
```

> Ejemplo de Archivo .env

```python
import os
from dotenv import load_dotenv

# Cargar variables de entorno desde el archivo .env
load_dotenv()

# Acceder a las variables de entorno
db_host = os.getenv("DB_HOST")
db_user = os.getenv("DB_USER")
db_password = os.getenv("DB_PASSWORD")

# Utilizar las variables de entorno
print(f"Host de la base de datos: {db_host}")
print(f"Usuario de la base de datos: {db_user}")
print(f"Contraseña de la base de datos: {db_password}")
```

> Código para Lectura de Variables de Entorno

## Escritura de Variables de Entorno en un Archivo .env

La escritura directa de variables de entorno en un archivo `.env` generalmente no es recomendada, ya que este archivo a menudo contiene información sensible como contraseñas. En su lugar, se sugiere editar el archivo manualmente o utilizar herramientas seguras para la configuración.

```python
from dotenv import set_key, unset_key
from pathlib import Path

env_path = Path('.') / '.env'

# cambiar variable
set_key(env_path, 'DB_HOST', 'localweb')
set_key(env_path, 'DB_USER', 'invited')
set_key(env_path, 'DB_PASSWORD', 'None')

# eliminar variable
unset_key(env_path, 'DB_PASSWORD')
```
