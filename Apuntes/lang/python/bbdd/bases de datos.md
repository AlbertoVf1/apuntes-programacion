---
date: 2024-07-10T12:33
title: Bases de datos
author: AlbertoVf
tags: ['sqlserver','mysql','postgresql','bbdd','microsoft']
resume: Apuntes de Bases de datos con Python
---


# Bases de datos en Python

1. SQL Server: Para trabajar con bases de datos SQL Server en Python, puedes utilizar la biblioteca `pyodbc`.

2. Asegúrate de tener instalada la biblioteca `psycopg2` para conectarte a la base de datos PostgreSQL

3. Aquí te presento un documento detallado con ejemplos sobre cómo trabajar con bases de datos MySQL en Python utilizando la biblioteca `mysql-connector-python`.

```bash
pip install pyodbc
pip install mysql-connector-python
pip install psycopg2
```

## Conexión a la Base de Datos

```python
import pyodbc

# Conectar a la base de datos
conn = pyodbc.connect(
    'DRIVER={SQL Server};'
    'SERVER=tu_servidor;'
    'DATABASE=tu_base_de_datos;'
    'UID=tu_usuario;'
    'PWD=tu_contraseña;'
)

# Crear un cursor para ejecutar comandos SQL
cursor = conn.cursor()
```

```python
import mysql.connector

# Conectar a la base de datos
conn = mysql.connector.connect(
    host="tu_host",
    user="tu_usuario",
    password="tu_contraseña",
    database="tu_base_de_datos"
)

# Crear un cursor para ejecutar comandos SQL
cursor = conn.cursor()
```

```python
import psycopg2

# Conectar a la base de datos
conn = psycopg2.connect(
    host="tu_host",
    database="tu_base_de_datos",
    user="tu_usuario",
    password="tu_contraseña"
)

# Crear un cursor para ejecutar comandos SQL
cursor = conn.cursor()
```

## Creación de Tablas

```python
# Ejemplo de creación de una tabla
create_table_query = '''
    CREATE TABLE empleados (
        id INT PRIMARY KEY,
        nombre VARCHAR(100),
        salario DECIMAL(10, 2)
    );
'''

cursor.execute(create_table_query)
conn.commit()
```

```python
# Ejemplo de creación de una tabla postgresql
create_table_query = '''
    CREATE TABLE IF NOT EXISTS empleados (
        id SERIAL PRIMARY KEY,
        nombre VARCHAR(100),
        salario DECIMAL(10, 2)
    );
'''

cursor.execute(create_table_query)
conn.commit()
```

## Inserción de Datos

```python
# Ejemplo de inserción de datos
insert_query = '''
    INSERT INTO empleados (id, nombre, salario) VALUES
    (1, 'Juan Pérez', 50000.00),
    (2, 'María López', 60000.00);
'''

cursor.execute(insert_query)
conn.commit()
```

## Consulta de Datos

```python
# Ejemplo de consulta de datos
select_query = "SELECT * FROM empleados;"

cursor.execute(select_query)
records = cursor.fetchall()

for record in records:
    print(record)
```

## Actualización de Datos

```python
# Ejemplo de actualización de datos
update_query = "UPDATE empleados SET salario = 55000.00 WHERE nombre = 'Juan Pérez';"

cursor.execute(update_query)
conn.commit()
```

## Eliminación de Datos

```python
# Ejemplo de eliminación de datos
delete_query = "DELETE FROM empleados WHERE nombre = 'María López';"

cursor.execute(delete_query)
conn.commit()
```

## Cierre de la Conexión

```python
# Cerrar el cursor y la conexión
cursor.close()
conn.close()
```
