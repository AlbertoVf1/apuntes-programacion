---
date: 2023-12-30T13:23
title: bases de datos sqlite
author: AlbertoVf
tags: ['bbdd','datos','sqlite']
resume: Base de datos Sqlite con Python
---


# Bases de Datos SQLite en Python

SQLite es un sistema de gestión de bases de datos relacional incorporado en Python. Este documento proporciona una introducción detallada y ejemplos sobre cómo trabajar con bases de datos SQLite en Python.

## Instalación y Conexión a SQLite

Antes de comenzar, asegúrate de tener instalada la biblioteca SQLite para Python. Puedes instalarla utilizando el siguiente comando:

```bash
pip install sqlite3
```

Ahora, vamos a crear una base de datos SQLite y conectarla:

```python
import sqlite3

# Conectar a la base de datos (creará un archivo si no existe)
conexion = sqlite3.connect("mi_base_de_datos.db")

# Crear un objeto cursor para ejecutar consultas SQL
cursor = conexion.cursor()

# Cerrar la conexión cuando hayas terminado
conexion.close()
```

## Creación de Tablas

Ahora que hemos conectado la base de datos, creemos una tabla y agreguemos algunos datos:

```python
import sqlite3

# Conectar a la base de datos
conexion = sqlite3.connect("mi_base_de_datos.db")
cursor = conexion.cursor()

# Crear una tabla
cursor.execute('''
    CREATE TABLE IF NOT EXISTS usuarios (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        nombre TEXT NOT NULL,
        edad INTEGER
    )
''')

# Insertar datos
cursor.execute("INSERT INTO usuarios (nombre, edad) VALUES (?, ?)", ("Alice", 25))
cursor.execute("INSERT INTO usuarios (nombre, edad) VALUES (?, ?)", ("Bob", 30))

# Guardar los cambios y cerrar la conexión
conexion.commit()
conexion.close()
```

En este ejemplo, hemos creado una tabla llamada `usuarios` con columnas `id`, `nombre` y `edad`. Luego, hemos insertado dos registros en la tabla.

## Consultas y Recuperación de Datos

Vamos a realizar algunas consultas y recuperar datos de la base de datos:

```python
import sqlite3

# Conectar a la base de datos
conexion = sqlite3.connect("mi_base_de_datos.db")
cursor = conexion.cursor()

# Consulta simple
cursor.execute("SELECT * FROM usuarios")
usuarios = cursor.fetchall()

# Imprimir resultados
for usuario in usuarios:
    print(usuario)

# Consulta con condiciones
cursor.execute("SELECT * FROM usuarios WHERE edad > ?", (28,))
usuarios_mayores_de_28 = cursor.fetchall()

print("Usuarios mayores de 28 años:")
for usuario in usuarios_mayores_de_28:
    print(usuario)

# Cerrar la conexión
conexion.close()
```

En este ejemplo, hemos realizado una consulta simple para recuperar todos los usuarios e otra consulta para recuperar usuarios mayores de 28 años.

## Actualización y Eliminación de Datos

Vamos a actualizar y eliminar algunos datos:

```python
import sqlite3

# Conectar a la base de datos
conexion = sqlite3.connect("mi_base_de_datos.db")
cursor = conexion.cursor()

# Actualizar datos
cursor.execute("UPDATE usuarios SET edad = ? WHERE nombre = ?", (26, "Alice"))

# Eliminar datos
cursor.execute("DELETE FROM usuarios WHERE nombre = ?", ("Bob",))

# Guardar los cambios y cerrar la conexión
conexion.commit()
conexion.close()
```

En este ejemplo, hemos actualizado la edad de "Alice" a 26 y luego eliminado el usuario "Bob" de la tabla.

## Uso de Context Managers

Es buena práctica usar context managers (`with`) al trabajar con bases de datos en Python para asegurarse de que las conexiones se cierren correctamente, incluso si ocurren excepciones:

```python
import sqlite3

# Usar un context manager
with sqlite3.connect("mi_base_de_datos.db") as conexion:
    cursor = conexion.cursor()

    # Realizar operaciones en la base de datos
    cursor.execute("INSERT INTO usuarios (nombre, edad) VALUES (?, ?)", ("Charlie", 22))
    # ...

# La conexión se cierra automáticamente al salir del bloque with
```
