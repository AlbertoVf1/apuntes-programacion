---
date: 2023-12-22T21:47
title: bucle for
author: AlbertoVf
tags: ['bucles', 'for']
resume: Bucle `for` en Python
---

# Bucle `for` en Python

El bucle `for` en Python es una estructura de control que permite iterar sobre secuencias, como listas, tuplas, cadenas, rangos o cualquier otro objeto iterable. Vamos a explorar la sintaxis y diferentes casos de uso del bucle `for` con ejemplos detallados.

## Sintaxis del Bucle `for`

```python
for variable in iterable:
    # Cuerpo del bucle
    # Se ejecuta para cada elemento en el iterable
```

- `variable`: La variable que toma el valor de cada elemento en cada iteración.
- `iterable`: La secuencia de elementos sobre la cual iterar.

## Ejemplos de Uso

```python
frutas = ['manzana', 'banana', 'cereza']

for fruta in frutas:
    print(fruta)
# $>
# manzana
# banana
# cereza
```

> Iterar sobre una lista

```python
mensaje = "Hola, Python!"

for letra in mensaje:
    print(letra)
# $>
# H
# o
# l
# a
# ,

# P
# y
# t
# h
# o
# n
# !
```

> Iterar sobre una cadena

```python
for i in range(5):
    print(i)
# $>
# 0
# 1
# 2
# 3
# 4
```

> Utilizar `range` para generar secuencias numéricas

```python
coordenadas = [(1, 2), (3, 4), (5, 6)]

for punto in coordenadas:
    x, y = punto
    print(f'Coordenada x: {x}, Coordenada y: {y}')
# $>
# Coordenada x: 1, Coordenada y: 2
# Coordenada x: 3, Coordenada y: 4
# Coordenada x: 5, Coordenada y: 6
```

> Iterar sobre una tupla de tuplas

```python
frutas = ['manzana', 'banana', 'cereza']

for indice, fruta in enumerate(frutas):
    print(f'Índice: {indice}, Valor: {fruta}')
# $>
# Índice: 0, Valor: manzana
# Índice: 1, Valor: banana
# Índice: 2, Valor: cereza
```

> Uso de `enumerate` para obtener índices y valores
