---
date: 2023-12-22T21:47
title: bucle while
author: AlbertoVf
tags: ['bucles', 'while']
resume: Bucle `while` en Python
---

# Bucle `while` en Python

El bucle `while` en Python es una estructura de control que permite ejecutar un bloque de código mientras una condición sea verdadera. A continuación, se presenta una guía detallada con ejemplos para entender el uso del bucle `while`.

## Sintaxis del Bucle `while`

```python
while condicion:
    # Cuerpo del bucle
    # Se ejecuta mientras la condición sea verdadera
```

- `condicion`: La expresión booleana que determina si el bucle debe continuar ejecutándose.

## Ejemplos de Uso

```python
contador = 0

while contador < 5:
    print(contador)
    contador += 1

# $>
# 0
# 1
# 2
# 3
# 4
```

> Bucle `while` básico

```python
numero = 1

while True:
    print(numero)
    numero += 1
    if numero > 5:
        break
# $>
# 1
# 2
# 3
# 4
# 5
```

> Uso de `break` para salir del bucle

```python
contador = 0

while contador < 3:
    print(f'Iteración {contador}')
    contador += 1
else:
    print('El bucle ha terminado')
# $>
# Iteración 0
# Iteración 1
# Iteración 2
# El bucle ha terminado
```

> Bucle `while` con `else`

```python
contador = 0

while contador < 5:
    contador += 1
    if contador == 3:
        continue
    print(contador)
# $>
# 1
# 2
# 4
# 5
```

> Uso de `continue` para saltar a la siguiente iteración

```python
nombre = ''

while not nombre:
    nombre = input('Ingrese su nombre: ')

print(f'Bienvenido, {nombre}!')
```

> Bucle `while` con entrada de usuario y validación. En este ejemplo, el bucle seguirá pidiendo al usuario que ingrese su nombre hasta que se proporcione una entrada no vacía.
