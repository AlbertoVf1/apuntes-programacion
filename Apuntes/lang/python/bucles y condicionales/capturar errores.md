---
date: 2023-12-24T14:30
title: capturar errores
author: AlbertoVf
tags: ['condicional', 'exception', 'try-except', 'finally']
resume: Manejo de Excepciones en Python
---

# Manejo de Excepciones en Python

En Python, el manejo de excepciones se realiza mediante las estructuras `try`, `except`, y opcionalmente `finally`. Estas estructuras permiten controlar y responder a errores durante la ejecución de un programa de manera elegante. A continuación, se proporciona una guía detallada con ejemplos de uso.

## Sintaxis de `try`, `except`, y `finally`

```python
try:
    # Código susceptible a errores
    # ...
except ExcepcionTipo1 as alias1:
    # Manejo de ExcepcionTipo1
    # ...
except ExcepcionTipo2 as alias2:
    # Manejo de ExcepcionTipo2
    # ...
else:
    # Código a ejecutar si no hay excepciones
    # ...
finally:
    # Código que siempre se ejecutará, con o sin excepciones
    # ...
```

- `try`: Contiene el código que podría generar una excepción.
- `except`: Define bloques de código que se ejecutarán si se detecta una excepción específica.
- `else`: Contiene código a ejecutar si no se producen excepciones en el bloque `try`.
- `finally`: Contiene código que se ejecutará siempre, independientemente de si se detectan excepciones o no.

### Ejemplos de Uso

```python
try:
    resultado = 10 / 0
except ZeroDivisionError as error:
    print(f'Error: {error}')
# Error: division by zero
```

> Manejo básico de una excepción

```python
try:
    x = int(input('Ingrese un número: '))
    resultado = 10 / x
except ValueError as error:
    print(f'Error de valor: {error}')
except ZeroDivisionError as error:
    print(f'Error de división por cero: {error}')
else:
    print(f'Resultado: {resultado}')
```

> Manejo de múltiples excepciones

```python
try:
    x = int(input('Ingrese un número: '))
    resultado = 10 / x
except ValueError as error:
    print(f'Error de valor: {error}')
except ZeroDivisionError as error:
    print(f'Error de división por cero: {error}')
else:
    print(f'Resultado: {resultado}')
```

> Uso de `else` para código sin excepciones

```python
try:
    archivo = open('archivo.txt', 'r')
    contenido = archivo.read()
    print(contenido)
except FileNotFoundError as error:
    print(f'Error de archivo no encontrado: {error}')
finally:
    if 'archivo' in locals():
        archivo.close()
```

> Uso de `finally`
