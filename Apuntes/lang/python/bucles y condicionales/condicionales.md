---
date: 2023-12-24T14:32
title: condicionales
author: AlbertoVf
tags: ['condicional', 'if-else-elif']
resume: Condicionales en Python
---

# Sintaxis de `if`, `else` y `elif`

```python
if condicion_1:
    # Bloque de código si la condición_1 es verdadera
    # ...
elif condicion_2:
    # Bloque de código si la condicion_1 es falsa y la condicion_2 es verdadera
    # ...
elif condicion_3:
    # Bloque de código si la condicion_1 y la condicion_2 son falsas y la condicion_3 es verdadera
    # ...
else:
    # Bloque de código si todas las condiciones anteriores son falsas
    # ...
```

- `if`: Se evalúa si una condición es verdadera.
- `elif` (else if): Se evalúa si la condición anterior (`if` o `elif`) es falsa y esta nueva condición es verdadera.
- `else`: Se ejecuta si todas las condiciones anteriores (`if` y `elif`) son falsas.

## Ejemplos de Uso

```python
edad = 18

if edad >= 18:
    print("Eres mayor de edad.")
else:
    print("Eres menor de edad.")

# $> Eres mayor de edad.
```

> Condición `if` simple

```python
puntaje = 75

if puntaje >= 90:
    print("Calificación: A")
elif puntaje >= 80:
    print("Calificación: B")
elif puntaje >= 70:
    print("Calificación: C")
else:
    print("Calificación: D")
# $> Calificación: C
```

> Condiciones `if` y `elif`

```python
numero = 10
resultado = "Par" if numero % 2 == 0 else "Impar"
print(resultado)

# $>Par
```

> Condición `if` con operador ternario

```python
temperatura = 25

if temperatura > 30:
    print("Hace calor.")
    if temperatura > 35:
        print("¡Y mucho calor!")
elif temperatura > 20:
    print("Temperatura agradable.")
else:
    print("Hace frío.")
# $> Temperatura agradable.
```

> Anidamiento de condicionales
