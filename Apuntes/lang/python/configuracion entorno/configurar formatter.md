---
date: 2023-12-23T21:07
title: configurar formatter
author: AlbertoVf
tags: ['entorno', 'proyecto', 'formateador', 'black']
resume: Configuración de un formateador de código
---

# Configuración de un Formmater

Para formatear el código Python, un formateador comúnmente utilizado es `black`. `Black` es una herramienta de formateo que sigue un conjunto estricto de reglas y busca producir un estilo de código consistente y limpio. Aquí hay un paso a paso para configurar `black`:

## Instalación de `black`

Abre tu terminal y ejecuta el siguiente comando para instalar `black` usando pip:

```bash
pip install black
```

## Configuración del archivo (opcional)

Puedes crear un archivo de configuración llamado `pyproject.toml` en la raíz de tu proyecto. Este archivo es compatible con PEP 518 y es el formato preferido para la configuración de `black`. Aquí hay un ejemplo básico:

```toml
[tool.black]
line-length = 88
target-version = ['py36']
exclude = '''
/(
  \.git
  | \.hg
  | \.mypy_cache
  | \.tox
  | \.venv
  | _build
  | buck-out
  | build
  | dist
)/
'''
```

Este archivo configura la longitud máxima de línea en 88 caracteres y establece la versión mínima de Python en 3.6. También especifica algunos directorios que deben excluirse del formateo.

## Uso básico

Para formatear tu código con `black`, simplemente ejecuta el siguiente comando en la terminal:

```bash
black .
```

> Esto formateará todos los archivos Python en el directorio actual y sus subdirectorios.

## Integración con un entorno de desarrollo (IDE)

Similar a lo que hicimos con `flake8`, puedes integrar `black` en tu entorno de desarrollo. Si usas VSCode, puedes buscar la extensión "Python" y elegir una que admita `black`.

## Automatización con herramientas de construcción

Integra `black` en tu flujo de trabajo de construcción para garantizar que el código se formatee automáticamente antes de cada confirmación o ejecución de pruebas.

Con estos pasos, habrás configurado `black` como tu formateador de código Python. Recuerda que el formateo automático puede cambiar la apariencia de tu código, así que siempre es bueno asegurarse de que todo sigue funcionando correctamente después de aplicar el formateo
