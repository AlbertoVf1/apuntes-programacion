---
date: 2023-12-23T21:01
title: configurar linter
author: AlbertoVf
tags: ['entorno', 'proyecto', 'linter', 'flake8']
resume: Configurar linter en python
---

# Configurar linter en python

## Instalación de flake8

Abre tu terminal y ejecuta el siguiente comando para instalar flake8 usando pip:

```bash
pip install flake8
```

> Esto instalará flake8 y sus dependencias.

## Configuración del (opcional)

Puedes crear un archivo de configuración para flake8 si deseas personalizar algunas reglas. Crea un archivo llamado `flake8.ini` o `setup.cfg` en la raíz de tu proyecto. Aquí hay un ejemplo básico de `flake8.ini`:

```ini
[flake8]
max-line-length = 120
exclude = .git, __pycache__, venv
```

### Configuración `flake8.ini`

El archivo `flake8.ini` (o `setup.cfg`, ya que ambos nombres son aceptados) es utilizado para configurar las opciones y reglas específicas de flake8 en un proyecto de Python. Aquí hay una explicación básica de algunos parámetros comunes que puedes incluir en este archivo:

1. **[flake8]**: Este encabezado define las opciones de configuración específicas de flake8.

2. **max-line-length**: Establece la longitud máxima permitida para una línea de código. Si una línea excede este límite, flake8 generará una advertencia.

    ```ini
    max-line-length = 120
    ```

3. **exclude**: Lista de patrones que especifican los directorios o archivos que se deben excluir del análisis de flake8. Generalmente se utilizan para ignorar directorios como `.git`, `__pycache__`, o entornos virtuales (`venv`).

    ```ini
    exclude = .git, __pycache__, venv
    ```

4. **ignore**: Lista de códigos de errores específicos que se deben ignorar. Puedes usar esto si hay reglas particulares que prefieres no aplicar en tu proyecto.

    ```ini
    ignore = E226,E302,E41
    ```

5. **max-complexity**: Establece el límite para la complejidad ciclomática. Si el código tiene una complejidad mayor que este límite, se generará una advertencia.

    ```ini
    max-complexity = 10
    ```

Estos son solo ejemplos básicos. Puedes consultar la documentación oficial de flake8 para obtener información más detallada sobre las opciones de configuración disponibles: [Configuración de flake8](https://flake8.pycqa.org/en/latest/user/configuration.html)

Recuerda que la configuración específica puede variar según tus preferencias y las necesidades de tu proyecto, así que ajusta estos valores según tus requisitos.

## Uso básico

Ahora, para ejecutar flake8 en tu proyecto, ve a la carpeta que contiene tu código y ejecuta el siguiente comando:

```bash
flake8
```

> Esto analizará tu código en busca de posibles problemas y estilo, mostrando los resultados en la terminal.

## Integración con un entorno de desarrollo (IDE)

Si estás utilizando un entorno de desarrollo como VSCode, puedes instalar una extensión que integre flake8 para obtener sugerencias y advertencias directamente en el editor. Busca la extensión "Python" en el mercado de extensiones de VSCode y elige una que admita flake8.

## Automatización con herramientas de construcción

Puedes integrar flake8 en tu flujo de trabajo de construcción (por ejemplo, usando herramientas como `pre-commit` o `tox`) para garantizar que el código se verifique automáticamente antes de cada confirmación o ejecución de pruebas.

¡Y eso es básicamente todo! Configurando un linter como flake8, estarás en camino de escribir código más limpio y mantener un estilo consistente en tus proyectos Python. ¡Espero que esta guía te haya sido útil
