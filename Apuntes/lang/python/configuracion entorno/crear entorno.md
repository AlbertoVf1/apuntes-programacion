---
date: 2023-12-23T13:29
title: crear entorno
author: AlbertoVf
tags: ['entorno', 'proyecto']
resume: Crear entorno de proyecto
---

# Configurar entorno virtual en un proyecto

## Instalar virtualenv

```bash
pip install virtualenv
```

> Abre tu terminal (o línea de comandos) y ejecuta el siguiente comando para instalar `virtualenv`:

## Crear un directorio para tu proyecto

```bash
mkdir mi_proyecto
cd mi_proyecto
```

> Crea un directorio para tu proyecto y navega a él en la terminal:

## Crear un entorno virtual

Ejecuta el siguiente comando para crear un entorno virtual dentro de tu directorio de proyecto:
Esto creará un directorio llamado `venv` que contendrá el entorno virtual.

```bash
virtualenv venv
```

## Activar el entorno virtual

Dependiendo de tu sistema operativo, el comando para activar el entorno virtual varía:

```batch
.\venv\Scripts\activate
```

> En Windows

```bash
source venv/bin/activate
```

>En macOS y Linux

Después de ejecutar este comando, verás el nombre de tu entorno virtual en la línea de comandos, indicando que estás dentro del entorno virtual.

## Instalar paquetes dentro del entorno virtual

Ahora que estás en tu entorno virtual, puedes instalar paquetes específicos para tu proyecto. Por ejemplo:

```bash
pip install nombre_del_paquete
```

> Instalacion usando pip

```bash
venv/bin/pip install nombre_del_paquete
```

> Instalacion usando `pip` del entorno virtual

## Desactivar el entorno virtual

Cuando hayas terminado de trabajar en tu proyecto, puedes desactivar el entorno virtual con el siguiente comando:

```bash
deactivate
```

> Esto te devolverá al entorno de Python global
