---
date: 2023-12-23T20:58
title: crear test
author: AlbertoVf
tags: ['entorno', 'proyecto', 'tests']
resume: Crear tests en un proyecto python
---

# Crear tests en un proyecto python

Crear pruebas (tests) en Python es una práctica fundamental para garantizar que tu código funcione correctamente y para detectar posibles problemas cuando realices cambios. Python tiene un módulo incorporado llamado `unittest` que facilita la escritura y ejecución de pruebas. Aquí hay una guía básica para crear tests en Python.

## Estructura de un Proyecto de Pruebas

Supongamos que tienes el siguiente código en un módulo llamado `mis_funciones.py`:

```python
# mis_funciones.py

def sumar(a, b):
    return a + b

def restar(a, b):
    return a - b
```

## Escribir Tests Utilizando `unittest`

1. **Crear un Módulo de Pruebas:** Crea un nuevo archivo, por ejemplo, `test_mis_funciones.py`, para tus pruebas.

2. **Importar `unittest` y el Módulo que Estás Probando**

    ```python
    import unittest
    from mis_funciones import sumar, restar
    ```

3. **Crear Clase de Pruebas Derivada de `unittest.TestCase`**

    ```python
    class TestMisFunciones(unittest.TestCase):
        pass
    ```

4. **Escribir Métodos de Pruebas:** Agrega métodos que comiencen con "test" en tu clase de pruebas. Estos métodos deben usar aserciones para verificar que el código funciona como se espera.

    ```python
    class TestMisFunciones(unittest.TestCase):
        def test_sumar(self):
            self.assertEqual(sumar(2, 3), 5)

        def test_restar(self):
            self.assertEqual(restar(5, 2), 3)
    ```

   > En este ejemplo, `assertEqual` verifica si el resultado de la función es igual al valor esperado.

5. **Ejecutar las Pruebas:** Puedes ejecutar tus pruebas utilizando el siguiente comando en la terminal:

    ```bash
    python -m unittest test_mis_funciones.py
    ```

   Asegúrate de estar en el directorio que contiene tus archivos de código y de pruebas.

## Aserciones Comunes en `unittest`

- `assertEqual(a, b)`: Verifica si `a` es igual a `b`.
- `assertNotEqual(a, b)`: Verifica si `a` no es igual a `b`.
- `assertTrue(x)`: Verifica si `x` es verdadero.
- `assertFalse(x)`: Verifica si `x` es falso.
- `assertIn(a, b)`: Verifica si `a` está en `b`.
- `assertNotIn(a, b)`: Verifica si `a` no está en `b`.

## Organización de Pruebas en Suites

Si tienes muchas pruebas, puedes organizarlas en suites. Puedes crear una clase derivada de `unittest.TestSuite` y agregar las pruebas a la suite.

## Uso de `setUp` y `tearDown`

Puedes usar los métodos `setUp` y `tearDown` para configurar y limpiar recursos antes y después de cada prueba.

```python
class TestMisFunciones(unittest.TestCase):
    def setUp(self):
        # Configurar recursos para las pruebas

    def tearDown(self):
        # Limpiar recursos después de las pruebas
```

Al seguir estas prácticas, puedes asegurarte de que tu código funcione correctamente y que cualquier cambio futuro no rompa funcionalidades existentes. La documentación oficial de [`unittest`](https://docs.python.org/3/library/unittest.html) ofrece más detalles sobre las aserciones y otras características avanzadas
