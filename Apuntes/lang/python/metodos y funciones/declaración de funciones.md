---
date: 2023-12-20T22:08
title: declaración de funciones
author: AlbertoVf
tags: ['funciones', 'predefinidas', 'lambda', 'def']
resume: Métodos y funciones en Python
---


# Métodos y Funciones en Python

En Python, tanto los métodos como las funciones son bloques de código reutilizables que realizan tareas específicas. La principal diferencia entre ellos radica en su relación con los objetos.

## Funciones

- Las funciones son bloques de código independientes que pueden aceptar parámetros y devolver un resultado.
- Se definen con la palabra clave `def`.
- Pueden ser llamadas con o sin argumentos, dependiendo de cómo se definan.

```python
def suma(a, b):
	return a + b

resultado = suma(3, 5)
print(resultado)  # Salida: 8
```

## Métodos

- Los métodos son funciones que están asociadas con un objeto específico y se llaman en ese objeto.
- Se llaman utilizando la notación de punto (`objeto.metodo()`).

```python
lista = [1, 2, 3]
lista.append(4)
print(lista)  # Salida: [1, 2, 3, 4]
```

## Lambda

Las funciones lambda son funciones anónimas y pequeñas que pueden tener cualquier número de parámetros, pero solo pueden tener una expresión. Se definen con la palabra clave `lambda`.

### Sintaxis

```python
lambda argumentos: expresion
```

```python
cuadrado = lambda x: x**2
print(cuadrado(5))  # Salida: 25
```

Las funciones lambda son útiles cuando se necesita una función simple para un propósito específico, como en la función `map`:
En este ejemplo, la función lambda se utiliza para elevar al cuadrado cada elemento de la lista.

```python
numeros = [1, 2, 3, 4, 5]
cuadrados = list(map(lambda x: x**2, numeros))
print(cuadrados)  # Salida: [1, 4, 9, 16, 25]
```

En resumen, los métodos y funciones son bloques de código reutilizables en Python, mientras que las funciones lambda son funciones anónimas y pequeñas que se utilizan principalmente para situaciones donde se requiere una función simple y rápida.
