---
date: 2023-12-25T12:00
title: funciones decorator
author: AlbertoVf
tags: ['funciones', 'decorators']
resume: Uso de decorators en Python
---

# Uso de Decoradores en Python

Los decoradores son una característica poderosa de Python que permite modificar o extender el comportamiento de funciones o métodos sin cambiar su código fuente. Aquí tienes un documento detallado con ejemplos del uso de decoradores en Python:

## Sintaxis Básica de un Decorador

```python
def decorador(funcion_original):
    def funcion_modificada(*args, **kwargs):
        # Código adicional antes de llamar a la función original
        resultado = funcion_original(*args, **kwargs)
        # Código adicional después de llamar a la función original
        return resultado
    return funcion_modificada

@decorador
def funcion_a_decorar():
    # Código de la función original
    pass

# Llamada a la función decorada
funcion_a_decorar()
```

## Ejemplos

```python
def mi_decorador(funcion_original):
    def funcion_modificada():
        print("Antes de llamar a la función original")
        funcion_original()
        print("Después de llamar a la función original")
    return funcion_modificada

@mi_decorador
def saludar():
    print("¡Hola!")

# Llamada a la función decorada
saludar()
```

> Decorador Simple

```python
def decorador_con_argumentos(arg1, arg2):
    def decorador(funcion_original):
        def funcion_modificada(*args, **kwargs):
            print(f"Argumentos del decorador: {arg1}, {arg2}")
            resultado = funcion_original(*args, **kwargs)
            return resultado
        return funcion_modificada
    return decorador

@decorador_con_argumentos("arg1_valor", "arg2_valor")
def funcion_decorada():
    print("¡Función decorada!")

# Llamada a la función decorada
funcion_decorada()
```

> Decorador con Argumentos

```python
import time

def medir_tiempo(funcion_original):
    def funcion_modificada(*args, **kwargs):
        inicio = time.time()
        resultado = funcion_original(*args, **kwargs)
        fin = time.time()
        print(f"Tiempo de ejecución: {fin - inicio} segundos")
        return resultado
    return funcion_modificada

@medir_tiempo
def operacion_larga():
    # Simulación de una operación que lleva tiempo
    time.sleep(2)
    print("Operación completa")

# Llamada a la función decorada
operacion_larga()
```

> Decorador para Medir Tiempo de Ejecución

```python
def validar_entrada(funcion_original):
    def funcion_modificada(*args, **kwargs):
        if all(isinstance(arg, int) for arg in args):
            resultado = funcion_original(*args, **kwargs)
            return resultado
        else:
            print("Error: Todas las entradas deben ser de tipo entero.")

    return funcion_modificada

@validar_entrada
def sumar_numeros(a, b):
    return a + b

# Llamada a la función decorada
resultado_suma = sumar_numeros(3, 5)
print("La suma es:", resultado_suma)
```

> Decorador para Validar Entradas

## Ventajas y Consideraciones

- **Reusabilidad de Código:** Los decoradores permiten aplicar funcionalidades comunes a múltiples funciones.
- **Modularidad:** Facilitan la separación de preocupaciones al dividir el código en unidades más pequeñas y especializadas.

## Limitaciones

- **Complejidad:** Decoradores más complejos pueden requerir una comprensión más profunda del sistema de decoradores en Python.

Los decoradores son una herramienta poderosa y flexible en Python que puede mejorar la legibilidad y mantenibilidad del código. Utilízalos con precaución y según sea necesario para evitar la complejidad innecesaria. ¡Espero que estos ejemplos te sean de utilidad!
