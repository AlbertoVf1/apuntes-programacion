---
date: 2023-12-25T12:07
title: funciones def
author: AlbertoVf
tags: ['funciones', 'def']
resume: Crear funciones con def
---

# Definición de Funciones en Python con `def`

La sintaxis básica de la declaración de funciones es la siguiente:

- `nombre_de_funcion`: Es el nombre que eliges para la función.
- `parametros`: Son los valores que la función recibe como entrada (pueden ser opcionales).
- `"""Documentación de la función"""`: Es una cadena de documentación opcional que describe lo que hace la función.
- `return resultado`: Especifica el valor que la función devuelve como resultado (puede ser opcional).

```python
def nombre_de_funcion(parametros):
    """Documentación de la función"""
    # Cuerpo de la función
    # ...
    return resultado
```

## Ejemplos

```python
def saludar():
    """Saluda al usuario"""
    print("¡Hola!")

# Llamada a la función
saludar()
```

> Función simple sin parámetros ni retorno

```python
def sumar(a, b):
    """Suma dos números"""
    resultado = a + b
    return resultado

# Llamada a la función
resultado_suma = sumar(3, 5)
print("La suma es:", resultado_suma)
```

> Función con parámetros

```python
def saludar_persona(nombre, saludo="Hola"):
    """Saluda a una persona con un saludo opcional"""
    print(saludo, nombre)

# Llamada a la función
saludar_persona("Juan")
saludar_persona("María", "Saludos")
    ```
> Función con parámetros y valor predeterminado

```python
def dividir(num1, num2):
    """Divide dos números y devuelve el cociente y el residuo"""
    cociente = num1 // num2
    residuo = num1 % num2
    return cociente, residuo

# Llamada a la función
resultado_division = dividir(10, 3)
print("Cociente:", resultado_division[0])
print("Residuo:", resultado_division[1])
```

> Función con múltiples retornos

```python
def sumar_varios(*args):
    """Suma una cantidad variable de números"""
    resultado = sum(args)
    return resultado

# Llamada a la función
suma_total = sumar_varios(1, 2, 3, 4, 5)
print("La suma total es:", suma_total)
```

> Función con argumentos variables (*args)

```python
def imprimir_info(**kwargs):
    """Imprime información proporcionada como argumentos clave"""
    for clave, valor in kwargs.items():
        print(f"{clave}: {valor}")

# Llamada a la función
imprimir_info(nombre="Juan", edad=30, ciudad="Ciudad de México")
```

> Función con argumentos clave variables (**kwargs)

Estos son solo algunos ejemplos básicos de cómo utilizar `def` para definir funciones en Python. Puedes adaptar estas estructuras según tus necesidades y crear funciones más complejas según lo requiera tu aplicación. ¡Espero que esto te sea útil!
