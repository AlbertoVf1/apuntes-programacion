---
date: 2023-12-25T12:03
title: funciones predefinidas
author: AlbertoVf
tags: ['funciones', 'predefinidas']
resume: funciones predefinidas en Python
---

# Funciones Predefinidas en Python

En Python, existen numerosas funciones predefinidas (built-in) que proporcionan funcionalidades esenciales para realizar diversas tareas. Aquí tienes un documento detallado con ejemplos del uso de algunas funciones predefinidas en Python:

## Funciones para Operaciones Básicas

```python
mensaje = "Hola, mundo!"
print(mensaje)
```

> La función `print()` se utiliza para imprimir mensajes en la consola.

```python
nombre = input("Ingrese su nombre: ")
print("Hola, " + nombre + "!")
```

> La función `input()` permite la entrada de datos desde el usuario.

## Funciones para Conversiones de Datos

```python
numero_entero = 10
numero_cadena = str(numero_entero)

cadena_numero = "15"
numero = int(cadena_numero)

numero_con_decimal = float(numero_entero)
```

> `str()`, `int()`, `float()` Estas funciones se utilizan para convertir entre diferentes tipos de datos.

## Funciones Matemáticas y Numéricas

```python
numero = -5
valor_absoluto = abs(numero)
print("Valor absoluto:", valor_absoluto)
```

> La función `abs()` devuelve el valor absoluto de un número.

```python
numero_decimal = 3.14159
numero_redondeado = round(numero_decimal, 2)
print("Número redondeado:", numero_redondeado)
```

> La función `round()` redondea un número al entero más cercano.

## Funciones para Listas y Secuencias

```python
lista = [1, 2, 3, 4, 5]
longitud_lista = len(lista)
print("Longitud de la lista:", longitud_lista)
```

> La función `len()` devuelve la longitud (número de elementos) de una secuencia.

```python
numeros = [10, 5, 8, 20, 3]
maximo = max(numeros)
minimo = min(numeros)
print("Máximo:", maximo)
print("Mínimo:", minimo)
```

> `max()`, `min()` Estas funciones devuelven el valor máximo o mínimo de una secuencia.

```python
numeros = [1, 2, 3, 4, 5]
suma = sum(numeros)
print("Suma de los números:", suma)
```

> La función `sum()` devuelve la suma de los elementos de una secuencia.

## Funciones de Cadenas de Texto

```python
cadena = "Python"
longitud_cadena = len(cadena)
print("Longitud de la cadena:", longitud_cadena)
```

> Además de usar `len()` en secuencias, también se puede usar para obtener la longitud de una cadena de texto.

```python
texto = "Hola, Mundo!"
mayusculas = texto.upper()
minusculas = texto.lower()
print("Mayúsculas:", mayusculas)
print("Minúsculas:", minusculas)
```

> `upper()`, `lower()`: Estas funciones convierten una cadena a mayúsculas o minúsculas.

## Funciones de Control de Flujo

```python
secuencia = range(5)
print(list(secuencia))  # Salida: [0, 1, 2, 3, 4]
```

> La función `range()` genera una secuencia de números.

```python
frutas = ["manzana", "banana", "cereza"]
for indice, valor in enumerate(frutas):
    print(f"Índice: {indice}, Valor: {valor}")
```

> La función `enumerate()` devuelve tuplas que contienen índices y valores de una secuencia.

## Funciones de Archivos

```python
# Abrir un archivo para escritura
with open("archivo.txt", "w") as archivo:
    archivo.write("Hola, archivo!")

# Abrir un archivo para lectura
with open("archivo.txt", "r") as archivo:
    contenido = archivo.read()
    print("Contenido del archivo:", contenido)
```

> `open()`, `read()`, `write()`: Estas funciones se utilizan para abrir, leer y escribir archivos.

## Funciones de Tiempo

```python
import time

tiempo_actual = time.time()
print("Tiempo actual:", tiempo_actual)

print("Esperando 2 segundos...")
time.sleep(2)
print("¡Listo!")
```

> La función `time()` devuelve la hora actual en segundos desde la época, y `sleep()` pausa la ejecución durante un período de tiempo dado.
