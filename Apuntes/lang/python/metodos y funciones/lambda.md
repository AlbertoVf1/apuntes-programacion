---
date: 2023-12-25T12:09
title: función lambda
author: AlbertoVf
tags: ['funciones', 'lambda']
resume: Uso de `lambda` en Python
---

# Uso de `lambda` en Python

En Python, `lambda` es una herramienta que permite crear funciones anónimas, es decir, funciones sin nombre. Estas funciones son útiles para situaciones en las que necesitas una función rápida y simple. Aquí te presento un documento detallado con ejemplos del uso de `lambda` en Python:

- `lambda`: Palabra clave para definir una función anónima.
- `argumentos`: Lista de parámetros que la función lambda toma como entrada.
- `expresion`: Una expresión que se evalúa y devuelve como resultado.

```python
lambda argumentos: expresion
```

## Ejemplos

```python
cuadrado = lambda x: x**2

# Uso de la función lambda
resultado = cuadrado(5)
print("El cuadrado es:", resultado)
```

> Función que devuelve el cuadrado de un número usando `lambda`

```python
suma = lambda a, b: a + b

# Uso de la función lambda
resultado_suma = suma(3, 7)
print("La suma es:", resultado_suma)
```

> Función que suma dos números usando `lambda`

```python
es_par = lambda x: True if x % 2 == 0 else False

# Uso de la función lambda
resultado_par = es_par(4)
print("¿Es par?", resultado_par)
```

> Función lambda en una expresión condicional

```python
numeros = [1, 2, 3, 4, 5]
cuadrados = list(map(lambda x: x**2, numeros))

# Uso de la función lambda con map
print("Cuadrados:", cuadrados)
```

> Utilizando `lambda` con funciones de orden superior (map)

```python
puntuaciones = [(80, 95), (90, 87), (88, 92)]
ordenado_por_segundo = sorted(puntuaciones, key=lambda x: x[1])

# Uso de la función lambda con sorted
print("Ordenado por segundo elemento:", ordenado_por_segundo)
```

> Ordenamiento de una lista de tuplas por el segundo elemento usando `lambda`

```python
numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
pares = list(filter(lambda x: x % 2 == 0, numeros))

# Uso de la función lambda con filter
print("Números pares:", pares)
```

> Filtrado de números pares usando `lambda`

## Ventajas y Consideraciones

- **Concisión:** `lambda` permite escribir funciones de una manera más concisa y compacta.
- **Sintaxis Simple:** Útil para funciones pequeñas y expresiones simples.
- **Funciones de Orden Superior:** Se puede usar con funciones de orden superior como `map`, `filter`, y `sorted`.

## Limitaciones

- **Limitada Complejidad:** No es adecuada para funciones más complejas que requieran múltiples expresiones o instrucciones.

Recuerda que mientras que `lambda` es útil en muchos casos, Python también ofrece la declaración `def` para definir funciones más extensas y complejas. Utiliza `lambda` cuando necesites una función pequeña y rápida. ¡Espero que estos ejemplos te sean de ayuda!
