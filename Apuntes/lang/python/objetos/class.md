---
date: 2023-12-31T12:13
title: class
author: AlbertoVf
tags: ['class', 'dataclass']
resume: Clases en Python
---

# Clases en Python

En Python, las clases son la piedra angular de la programación orientada a objetos (OOP). Permiten encapsular datos y comportamientos en un solo lugar, promoviendo la reutilización y la organización del código. A continuación, se presenta un documento detallado con ejemplos sobre el uso de clases en Python.

## Creación de una Clase Básica

```python
class Persona:
    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad

    def saludar(self):
        print(f"Hola, soy {self.nombre} y tengo {self.edad} años.")

# Crear una instancia de la clase Persona
persona1 = Persona("Juan", 30)

# Acceder a atributos y llamar a métodos
print(persona1.nombre)  # Juan
print(persona1.edad)    # 30
persona1.saludar()      # Hola, soy Juan y tengo 30 años.
```

En este ejemplo, la clase `Persona` tiene un constructor `__init__` que inicializa los atributos `nombre` y `edad`. También tiene un método `saludar` que imprime un saludo utilizando estos atributos.

## Creación de una clase Avanzada

### dataclass

Declaración de clase utilizando el decorator `@dataclass`. Se declara una clase que usa como constructor los 2 parámetros en el orden en el que se declaran

```python
from dataclasses import dataclass

@dataclass
class Class:
    property_1: int
    property_2: str
```

### Leer dict

Declaración de clase a partir de un dict. Se utiliza un método estático que recibe un `dict` para obtener las propiedades y retorna el objeto con las propiedades declaradas

```python
from dataclasses import dataclass
@dataclass
class Class:
    property_1: int
    property_2: str

    @staticmethod
    def from_dict(obj:dict)->'Class':
        _property_1 = str(obj.get('_property_1'))
        _property_2 = str(obj.get('_property_2'))
        return Class(_property_1,_property_2)
```

## Herencia en Clases

```python
class Empleado(Persona):
    def __init__(self, nombre, edad, salario):
        super().__init__(nombre, edad)
        self.salario = salario

    def trabajar(self):
        print(f"{self.nombre} está trabajando.")

# Crear una instancia de la clase Empleado
empleado1 = Empleado("Ana", 25, 50000)

# Heredar atributos y métodos de la clase Persona
print(empleado1.nombre)    # Ana
print(empleado1.edad)      # 25
empleado1.saludar()        # Hola, soy Ana y tengo 25 años.

# Utilizar atributos y métodos propios de la clase Empleado
print(empleado1.salario)   # 50000
empleado1.trabajar()        # Ana está trabajando.
```

En este ejemplo, la clase `Empleado` hereda de la clase `Persona`, aprovechando la reutilización de código y permitiendo la extensión de comportamiento.

## Atributos de Clase y Métodos Estáticos

```python
class CuentaBancaria:
    tasa_interes = 0.02  # Atributo de clase compartido por todas las instancias

    def __init__(self, saldo):
        self.saldo = saldo

    def aplicar_interes(self):
        self.saldo += self.saldo * self.tasa_interes

    @staticmethod
    def descripcion():
        print("Cuenta bancaria para gestionar saldos.")
```

En este ejemplo, `tasa_interes` es un atributo de clase compartido por todas las instancias de `CuentaBancaria`. El método `aplicar_interes` modifica el saldo según la tasa de interés. El método `descripcion` es un método estático que se puede llamar sin crear una instancia.

## Métodos Mágicos

```python
class Libro:
    def __init__(self, titulo, autor):
        self.titulo = titulo
        self.autor = autor

    def __str__(self):
        return f"{self.titulo} por {self.autor}"

    def __eq__(self, otro_libro):
        return self.titulo == otro_libro.titulo and self.autor == otro_libro.autor

# Crear instancias de la clase Libro
libro1 = Libro("Harry Potter", "J.K. Rowling")
libro2 = Libro("Harry Potter", "J.K. Rowling")

# Utilizar métodos mágicos
print(libro1)         # Harry Potter por J.K. Rowling
print(libro1 == libro2)  # True
```

En este ejemplo, `__str__` define una representación en cadena del objeto, y `__eq__` define cómo se compara la igualdad entre dos objetos de la clase `Libro`.

## Encapsulamiento

```python
class CajaFuerte:
    def __init__(self, codigo):
        self.__codigo = codigo  # Atributo privado

    def abrir(self, codigo_ingresado):
        if codigo_ingresado == self.__codigo:
            print("Caja fuerte abierta.")
        else:
            print("Código incorrecto.")

# Crear una instancia de la clase CajaFuerte
caja = CajaFuerte(1234)

# Intentar acceder directamente al atributo privado
# Esto generará un error: 'CajaFuerte' object has no attribute '__codigo'
# print(caja.__codigo)

# Utilizar el método público para abrir la caja fuerte
caja.abrir(1234)  # Caja fuerte abierta.
```

En este ejemplo, `__codigo` es un atributo privado que solo puede ser accedido desde dentro de la propia clase. Se accede a él mediante un método público (`abrir`).

## Conclusión

Las clases en Python permiten organizar y estructurar el código de manera eficiente, siguiendo los principios de la programación orientada a objetos. Puedes aprovechar la herencia, los métodos mágicos y el encapsulamiento para construir programas más modulares y reutilizables. La flexibilidad y simplicidad de la sintaxis de Python hacen que trabajar con clases sea accesible incluso para aquellos nuevos en la programación orientada a objetos.
