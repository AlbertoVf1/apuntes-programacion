---
date: 2023-12-30T13:25
title: enum
author: AlbertoVf
tags: ['Enum','class']
resume: Enum en Python
---

# Introducción a Enum en Python

`Enum` (Enumerated) es una clase que se encuentra en el módulo `enum` de Python. Proporciona una manera de crear enumeraciones, que son un conjunto de nombres simbólicos (miembros) vinculados a valores únicos e intencionales.

## Importar el módulo enum

```python
from enum import Enum, auto
```

## Creación básica de Enumeraciones

Puedes crear una enumeración definiendo una clase que herede de `Enum`. Los miembros de la enumeración son atributos de la clase.

```python
class EstadoCivil(Enum):
    SOLTERO = 1
    CASADO = 2
    DIVORCIADO = 3
    VIUDO = 4
```

```python
print(EstadoCivil.SOLTERO)  # EstadoCivil.SOLTERO
```

> Puedes acceder a los miembros de la enumeración utilizando la notación de puntos.

```python
for estado in EstadoCivil:
    print(estado)
```

> Iteración sobre los miembros de Enum: Puedes iterar sobre los miembros de la enumeración.

## Obtener el valor asociado a un miembro

```python
valor_soltero = EstadoCivil.SOLTERO.value
print(valor_soltero)  # 1
```

## Comparación de Enum

Los miembros de Enum son comparables entre sí.

```python
if EstadoCivil.SOLTERO == EstadoCivil.CASADO:
    print("Esto no se imprimirá")
```

## Uso de `auto()` para asignar valores automáticamente

Si no quieres asignar valores a los miembros manualmente, puedes utilizar `auto()`.

```python
class Color(Enum):
    ROJO = auto()
    VERDE = auto()
    AZUL = auto()
```

## Enumeración con atributos

Puedes agregar atributos a los miembros de Enum.

```python
from enum import Enum

class Tamaño(Enum):
    PEQUEÑO = "S", 1
    MEDIANO = "M", 2
    GRANDE = "L", 3

# Acceder a atributos
print(Tamaño.PEQUEÑO.value)  # ("S", 1)
print(Tamaño.PEQUEÑO.value[0])  # "S"
```

## Uso de Enum en una clase

Puedes usar Enum dentro de una clase para representar estados o categorías.

```python
class Animal(Enum):
    PERRO = "Perro"
    GATO = "Gato"

class Mascota:
    def __init__(self, nombre, tipo):
        self.nombre = nombre
        self.tipo = tipo

# Uso
mi_mascota = Mascota("Firulais", Animal.PERRO)
print(mi_mascota.tipo)  # Animal.PERRO
```
