---
date: 2023-12-30T13:31
title: herencia
author: AlbertoVf
tags: ['class','herencia']
resume: Uso de herencia en Python
---

# Herencia en Python

La herencia es un concepto fundamental en la programación orientada a objetos (OOP) que permite la creación de clases nuevas basadas en clases existentes. En Python, la herencia se implementa de manera sencilla y potente. A continuación, se presenta un documento detallado con ejemplos sobre la herencia en Python.

## Introducción a la Herencia

En OOP, la herencia permite que una clase (llamada clase derivada o subclase) herede atributos y métodos de otra clase (llamada clase base o superclase). Esto fomenta la reutilización del código y la creación de una jerarquía de clases.

```python
class Animal:
    def __init__(self, nombre):
        self.nombre = nombre

    def hacer_sonido(self):
        pass

class Perro(Animal):
    def hacer_sonido(self):
        return "Woof!"

class Gato(Animal):
    def hacer_sonido(self):
        return "Meow!"
```

> En este ejemplo, `Perro` y `Gato` son subclases de la clase base `Animal`. Comparten el atributo `nombre` y tienen sus propias implementaciones del método `hacer_sonido`.

## Acceso a Métodos y Atributos de la Clase Base

Las subclases pueden acceder a los métodos y atributos de la clase base. Para hacerlo, se utiliza la función `super()`.

```python
class Vehiculo:
    def __init__(self, marca):
        self.marca = marca

    def arrancar(self):
        print("El vehículo está arrancando.")

class Coche(Vehiculo):
    def __init__(self, marca, modelo):
        super().__init__(marca)
        self.modelo = modelo

    def arrancar(self):
        super().arrancar()
        print(f"El coche {self.modelo} está en movimiento.")
```

En este ejemplo, `Coche` hereda de `Vehiculo`, y al sobrescribir el método `arrancar`, se llama al método de la clase base utilizando `super()` antes de agregar la funcionalidad específica del coche.

## Herencia Múltiple

Python permite la herencia múltiple, donde una clase puede heredar de más de una clase.

```python
class A:
    def metodo_a(self):
        print("Método A")

class B:
    def metodo_b(self):
        print("Método B")

class C(A, B):
    pass

instancia_c = C()
instancia_c.metodo_a()  # Método A
instancia_c.metodo_b()  # Método B
```

En este ejemplo, la clase `C` hereda de las clases `A` y `B`, por lo que tiene acceso a los métodos de ambas.

## Herencia y Constructores

Es posible que quieras que la subclase tenga su propio constructor además del constructor de la clase base.

```python
class Figura:
    def __init__(self, color):
        self.color = color

class Circulo(Figura):
    def __init__(self, color, radio):
        super().__init__(color)
        self.radio = radio
```

La subclase `Circulo` llama al constructor de `Figura` usando `super()` y luego agrega su propio atributo `radio`.
