---
date: 2023-12-30T14:03
title: interface
author: AlbertoVf
tags: ['interface','class']
resume: Uso de interfaces en Python
---


# Uso de interfaces en Python

Python no tiene una palabra clave específica para declarar interfaces como en algunos lenguajes orientados a objetos. Sin embargo, puedes lograr la creación de interfaces utilizando clases abstractas y el módulo `abc` (Abstract Base Classes) en Python. Aunque no es exactamente lo mismo que en lenguajes que tienen soporte integrado para interfaces, las clases abstractas proporcionan una forma de definir una interfaz en Python.

Aquí hay un ejemplo de cómo puedes crear una "interfaz" en Python utilizando clases abstractas:

## Creacion de interfaz

```python
from abc import ABC, abstractmethod

# Definición de la interfaz
class Forma(ABC):
    @abstractmethod
    def area(self):
        pass

    @abstractmethod
    def perimetro(self):
        pass

# Implementación de la interfaz en una clase concreta
class Cuadrado(Forma):
    def __init__(self, lado):
        self.lado = lado

    def area(self):
        return self.lado ** 2

    def perimetro(self):
        return 4 * self.lado

# Otra implementación de la interfaz en una clase concreta
class Circulo(Forma):
    def __init__(self, radio):
        self.radio = radio

    def area(self):
        return 3.14 * self.radio ** 2

    def perimetro(self):
        return 2 * 3.14 * self.radio
```

En este ejemplo, la clase `Forma` se declara como una clase abstracta con dos métodos abstractos (`area` y `perimetro`). Luego, las clases concretas como `Cuadrado` y `Circulo` heredan de esta clase e implementan los métodos abstractos.

Cuando intentas instanciar una clase que no implementa todos los métodos abstractos de la interfaz, Python generará un error.

```python
# Esto generará un error ya que Cuadrado no implementa 'perimetro'
mi_cuadrado = Cuadrado(5)
```

Este enfoque proporciona una forma de definir interfaces en Python y asegurarte de que las clases concretas implementen todos los métodos necesarios. Sin embargo, ten en cuenta que en Python, el énfasis suele estar en la "interfaz implícita", lo que significa que se espera que las clases simplemente implementen ciertos métodos en lugar de heredar de una interfaz explícita. Esto es parte de la filosofía "duck typing" de Python, que se centra en el comportamiento en lugar de en la herencia de interfaces.
