---
date: 2023-12-30T14:32
title: polimorfismo
author: AlbertoVf
tags: ['polimorfismo','class']
resume: Polimorfismo en Python
---

# Polimorfismo en Python

El polimorfismo es un principio de la programación orientada a objetos que permite que objetos de diferentes clases sean tratados como objetos de una misma clase base. En Python, el polimorfismo se implementa de manera natural y es una de las características clave de la programación orientada a objetos. A continuación, se presenta un documento detallado con ejemplos sobre el polimorfismo en Python.

## Introducción al Polimorfismo

El polimorfismo permite que un objeto pueda tomar múltiples formas. En el contexto de la programación orientada a objetos, esto significa que un objeto puede ser tratado como un objeto de una clase base, incluso si es una instancia de una subclase.

### Ejemplo Básico de Polimorfismo

```python
class Animal:
    def sonido(self):
        pass

class Perro(Animal):
    def sonido(self):
        return "Woof!"

class Gato(Animal):
    def sonido(self):
        return "Meow!"

# Función que utiliza polimorfismo
def hacer_sonido(animal):
    return animal.sonido()

# Crear instancias de las clases
mi_perro = Perro()
mi_gato = Gato()

# Utilizar la función con diferentes instancias
print(hacer_sonido(mi_perro))  # Woof!
print(hacer_sonido(mi_gato))   # Meow!
```

En este ejemplo, la función `hacer_sonido` toma un objeto de tipo `Animal`, pero puede trabajar con instancias de las subclases `Perro` y `Gato`. Esto es posible debido al polimorfismo.

## Polimorfismo con Métodos y Funciones

El polimorfismo se manifiesta tanto en métodos de clases como en funciones que operan sobre objetos.

```python
class Rectangulo:
    def __init__(self, largo, ancho):
        self.largo = largo
        self.ancho = ancho

    def area(self):
        return self.largo * self.ancho

class Circulo:
    def __init__(self, radio):
        self.radio = radio

    def area(self):
        return 3.14 * self.radio**2

# Función que opera sobre diferentes tipos de objetos
def calcular_area(objeto):
    return objeto.area()

# Crear instancias de las clases
mi_rectangulo = Rectangulo(5, 3)
mi_circulo = Circulo(7)

# Utilizar la función con diferentes instancias
print(calcular_area(mi_rectangulo))  # 15
print(calcular_area(mi_circulo))     # 153.86
```

En este caso, `calcular_area` puede trabajar con instancias de `Rectangulo` y `Circulo` debido a que ambos implementan un método `area`.

## Polimorfismo con Métodos Mágicos

Los métodos mágicos (`__add__`, `__str__`, etc.) permiten que los objetos interactúen de ciertas maneras, facilitando el polimorfismo.

```python
class Punto:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, otro_punto):
        return Punto(self.x + otro_punto.x, self.y + otro_punto.y)

    def __str__(self):
        return f"({self.x}, {self.y})"

# Crear instancias de la clase
punto1 = Punto(1, 2)
punto2 = Punto(3, 4)

# Utilizar el operador de suma y el método str
resultado = punto1 + punto2
print(resultado)  # (4, 6)
```

En este ejemplo, el método `__add__` permite que los objetos de la clase `Punto` se sumen entre sí, mostrando así el polimorfismo en acción.

## Conclusión

El polimorfismo es una característica fundamental en la programación orientada a objetos que permite escribir código más flexible y reutilizable. En Python, se logra de manera natural a través de la capacidad de tratar objetos de diferentes clases como si fueran objetos de la misma clase base. El polimorfismo simplifica la implementación y mantenimiento del código, ya que puedes escribir funciones y métodos que operan sobre objetos genéricos, y estas funciones pueden ser utilizadas con instancias de diferentes clases.
