---
date: 2023-12-30T14:41
title: serializacion
author: AlbertoVf
tags: ['class','serializacion']
resume: Serializacion de objetos en Python
---

# Serialización de Objetos en Python

La serialización es el proceso de convertir un objeto o estructura de datos en un formato que pueda ser almacenado o transmitido, y luego reconstruido posteriormente. En Python, el módulo `pickle` proporciona funcionalidades para serializar y deserializar objetos. A continuación, se presenta un documento detallado con ejemplos de serialización de objetos en Python.

## Serialización Básica con Pickle

El módulo `pickle` permite serializar y deserializar objetos de Python. Aquí hay un ejemplo básico:

```python
import pickle

# Objeto a serializar
datos = {'nombre': 'Alice', 'edad': 30, 'ciudad': 'Wonderland'}

# Serialización
with open('datos_serializados.pkl', 'wb') as archivo:
    pickle.dump(datos, archivo)

# Deserialización
with open('datos_serializados.pkl', 'rb') as archivo:
    datos_deserializados = pickle.load(archivo)

print(datos_deserializados)
# Salida: {'nombre': 'Alice', 'edad': 30, 'ciudad': 'Wonderland'}
```

En este ejemplo, `pickle.dump()` se utiliza para escribir el objeto en un archivo binario (`'wb'`), y `pickle.load()` se utiliza para leer el objeto de vuelta.

## Serialización de Objetos Personalizados

Puedes serializar objetos personalizados implementando los métodos especiales `__getstate__` y `__setstate__` en tu clase.

```python
import pickle

class Persona:
    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad

    def __repr__(self):
        return f"Persona('{self.nombre}', {self.edad})"

    def __getstate__(self):
        # Retornar un diccionario con el estado que quieres serializar
        return {'nombre': self.nombre, 'edad': self.edad}

    def __setstate__(self, estado):
        # Restaurar el estado desde el diccionario
        self.__dict__.update(estado)

# Crear una instancia de la clase Persona
persona = Persona('Bob', 25)

# Serialización
with open('persona_serializada.pkl', 'wb') as archivo:
    pickle.dump(persona, archivo)

# Deserialización
with open('persona_serializada.pkl', 'rb') as archivo:
    persona_deserializada = pickle.load(archivo)

print(persona_deserializada)
# Salida: Persona('Bob', 25)
```

En este ejemplo, `__getstate__` retorna un diccionario con los datos que queremos serializar, y `__setstate__` restaura el estado desde ese diccionario.

## Uso de JSON para Serialización

El módulo `json` puede ser utilizado para serializar objetos a formato JSON, que es más legible y también puede ser interpretado por otros lenguajes.

```python
import json

# Objeto a serializar
datos = {'nombre': 'Alice', 'edad': 30, 'ciudad': 'Wonderland'}

# Serialización
datos_serializados = json.dumps(datos)

# Deserialización
datos_deserializados = json.loads(datos_serializados)

print(datos_deserializados)
# Salida: {'nombre': 'Alice', 'edad': 30, 'ciudad': 'Wonderland'}
```

## Serialización con Marshmallow (Para Estructuras más Complejas)

El paquete `marshmallow` proporciona una forma más flexible de serializar y deserializar objetos, especialmente útil para estructuras de datos más complejas.

```python
from marshmallow import Schema, fields

# Definir un esquema para la clase Persona
class PersonaSchema(Schema):
    nombre = fields.Str()
    edad = fields.Int()

# Crear una instancia de la clase Persona
persona = {'nombre': 'Charlie', 'edad': 28}

# Serialización
persona_schema = PersonaSchema()
datos_serializados = persona_schema.dump(persona)

# Deserialización
datos_deserializados = persona_schema.load(datos_serializados)

print(datos_deserializados)
# Salida: {'nombre': 'Charlie', 'edad': 28}
```

En este ejemplo, `PersonaSchema` define cómo se debe serializar y deserializar la clase `Persona`.

## Conclusión

La serialización de objetos es una técnica fundamental en la programación moderna, permitiendo almacenar y transmitir datos de manera eficiente. Python proporciona diversas opciones, como `pickle` y `json`, para satisfacer diferentes necesidades de serialización. La elección de la herramienta dependerá de factores como la complejidad de la estructura de datos y la interoperabilidad con otros lenguajes.
