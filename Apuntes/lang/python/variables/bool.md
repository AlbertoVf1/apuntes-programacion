---
date: 2023-12-21T22:24
title: variable bool
author: AlbertoVf
tags: ['variables', 'bool', 'True/False']
resume: Tipo `bool` en Python
---

# Tipo `bool` en Python

El tipo `bool` en Python es utilizado para representar valores booleanos, es decir, valores de verdad que pueden ser `True` o `False`.

```python
# Asignación directa
mi_variable = True

# Mediante expresiones lógicas
condicion = 5 > 3  # Esto asigna el valor True a la variable condición
```

## Operadores Lógicos

Python proporciona operadores lógicos que trabajan con valores booleanos. Algunos de ellos son:

- `and`: Devuelve `True` si ambas expresiones son verdaderas.
- `or`: Devuelve `True` si al menos una de las expresiones es verdadera.
- `not`: Devuelve la negación del valor booleano.

```python
a = True
b = False

resultado_and = a and b  # False
resultado_or = a or b    # True
resultado_not = not a    # False
```

## Comparaciones

Los operadores de comparación devuelven valores booleanos:

- `==`: Igual a
- `!=`: Diferente de
- `<`: Menor que
- `>`: Mayor que
- `<=`: Menor o igual que
- `>=`: Mayor o igual que

```python
x = 5
y = 10

igual_a = x == y       # False
diferente_de = x != y  # True
menor_que = x < y      # True
```

## Funciones que Devuelven `bool`

Algunas funciones o métodos devuelven valores booleanos:

```python
cadena = "Hola, mundo!"

# Método de cadena
contiene = "mundo" in cadena  # True

# Función built-in
es_numero = isinstance(x, int)  # True si x es de tipo int
```
