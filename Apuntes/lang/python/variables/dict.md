---
date: 2023-12-21T22:31
title: variable dict
author: AlbertoVf
tags: ['variables', 'colección', 'clave-valor']
resume: Tipo `dict` en Python
---

# Tipo `dict` en Python

El tipo `dict` en Python representa un diccionario, que es una colección no ordenada de pares clave-valor. Cada clave en un diccionario debe ser única, y se utiliza para acceder al valor asociado.

- Las claves en un diccionario deben ser únicas.
- Los diccionarios son mutables; puedes modificarlos después de su creación.
- Los diccionarios son muy eficientes para buscar valores basados en claves.

Los diccionarios son una estructura de datos versátil y poderosa en Python, utilizada para representar asociaciones clave-valor y estructuras de datos más complejas.

## Creacion de `dict`

```python
mi_dict = {"clave1": "valor1", "clave2": "valor2", "clave3": "valor3"}
```

> Usando llaves `{}`

```python
otro_dict = dict(nombre="Juan", edad=25, ciudad="Ejemploville")
```

> Con la Función `dict()`

```python
usuarios = {
    "usuario1": {"nombre": "Juan", "edad": 25},
    "usuario2": {"nombre": "María", "edad": 30}
}
```

> Diccionarios Anidados: Los diccionarios pueden contener otros diccionarios como valores.

## Iteración sobre un Diccionario

Puedes iterar sobre las claves, los valores o los pares clave-valor de un diccionario.

```python
# Iterar sobre las claves
for clave in mi_dict:
    print(clave)

# Iterar sobre los valores
for valor in mi_dict.values():
    print(valor)

# Iterar sobre los pares clave-valor
for clave, valor in mi_dict.items():
    print(f"Clave: {clave}, Valor: {valor}")
```

## Funciones y metodos relacionados con `dict`

```python
nombre = mi_dict["clave1"]  # Accede al valor asociado con la clave "clave1"
```

> Los elementos en un diccionario se acceden mediante las claves.

```python
mi_dict["nueva_clave"] = "nuevo_valor"  # Agrega un nuevo par clave-valor
```

> Agregar o Modificar Elementos:

```python
del mi_dict["clave2"]
```

> Elimina el par clave-valor con la clave "clave2"

```python
pertenece = "clave3" in mi_dict
```

> Verifica si "clave3" está en el diccionario

```python
todas_las_claves = mi_dict.keys()
```

> `keys()`: Devuelve una lista de todas las claves en el diccionario.

```python
todos_los_valores = mi_dict.values()
```

> `values()`: Devuelve una lista de todos los valores en el diccionario.

```python
todos_los_items = mi_dict.items()
```

> `items()`: Devuelve una lista de tuplas (clave, valor) para cada par en el diccionario.
