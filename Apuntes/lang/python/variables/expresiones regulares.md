---
date: 2023-12-23T13:06
title: expresiones regulares
author: AlbertoVf
tags: ['variables', 'expresión regular', 'cadena']
resume: Crear expresiones regulares en python
---

# Expresiones regulares en Python

Las expresiones regulares (también conocidas como regex o regexp) son patrones de búsqueda de texto que se utilizan para realizar operaciones de búsqueda y manipulación de cadenas de texto. En Python, el módulo `re` proporciona funciones para trabajar con expresiones regulares. Aquí tienes una guía básica del uso de expresiones regulares en Python.

## Importar el Módulo `re`

Primero, necesitas importar el módulo `re`:

```python
import re
```

## Patrones Básicos

- **`^`:** Coincide con el principio de la cadena.
- **`$`:** Coincide con el final de la cadena.
- **`.`:** Coincide con cualquier carácter excepto una nueva línea.
- **`*`:** Coincide con cero o más repeticiones del carácter anterior.
- **`+`:** Coincide con una o más repeticiones del carácter anterior.
- **`?`:** Coincide con cero o una repetición del carácter anterior.
- **`\`:** Se utiliza para escapar caracteres especiales.

## Funciones y metodos relacionados con `r`

```python
texto = "La programación en Python es genial"

resultado = re.search(r"Python", texto)
if resultado:
    print("Encontrado:", resultado.group())
else:
    print("No encontrado")
```

> Buscar si la palabra "Python" está en el texto

```python
texto = "Fecha de nacimiento: 21-12-1990"

# Extraer la fecha de nacimiento
resultado = re.search(r"(\d{2}-\d{2}-\d{4})", texto)

if resultado:
    print("Fecha de nacimiento:", resultado.group(1))
else:
    print("Fecha no encontrada")
```

> Uso de Grupos

```python
texto = "¡Hola, ¿cómo estás?!"

# Eliminar caracteres especiales y convertir a minúsculas
limpio = re.sub(r"[^a-zA-Z0-9 ]", "", texto)
limpio = limpio.lower()

print("Texto limpio:", limpio)
```

> Uso de Caracteres Especiales

```python
texto = "La programación en Python es divertida y Python es poderoso"

# Encontrar todas las ocurrencias de "Python"
ocurrencias = re.findall(r"Python", texto)

print("Ocurrencias:", ocurrencias)
```

> Uso de `re.findall`

```python
texto = "Manzanas,Naranjas,Peras,Uvas"

# Dividir el texto en una lista utilizando la coma como separador
frutas = re.split(r",", texto)

print("Frutas:", frutas)
```

> Uso de `re.split`
