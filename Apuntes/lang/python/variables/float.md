---
date: 2023-12-20T22:49
title: float
author: AlbertoVf
tags: ['variables', 'numérico', 'decimal']
resume: Tipo de dato `float` en Python
---

# Tipo `float` en Python

El tipo `float` se utiliza para representar números de punto flotante (números decimales) en Python. A continuación, se presenta un documento detallado con ejemplos sobre el tipo `float`:

```python
numero_decimal = 3.14
```

> Ejemplo de variable float

## Operaciones con `float`

```python
# Suma
resultado_suma = 2.5 + 1.5
# Resta
resultado_resta = 5.0 - 2.5
# Multiplicación
resultado_multiplicacion = 1.5 * 2
# División
resultado_division = 3.0 / 1.5
# Potencia
resultado_potencia = 2.0 ** 3
```

```python
# Asignación simple
x = 2.0
# Asignación con operación aritmética
x += 0.5  # Equivalente a x = x + 0.5
# Otras operaciones: -=, *=, /=
```

> Asignacion de valores

```python
# Convertir a float desde otro tipo
valor_entero = 42
valor_float_desde_int = float(valor_entero)
# Convertir a float desde string
valor_string = "3.14"
valor_float_desde_string = float(valor_string)
```

> Conversor de tipos

## Funciones y métodos relacionados con `float`

```python
# Obtener la parte entera de un float
parte_entera = int(3.75)
# Redondear un float
redondeo = round(4.6)
# Otras funciones: abs(), max(), min()
```

```python
es_float = isinstance(3.14, float)
```

> Verificar si una variable es de tipo float
