---
date: 2023-12-20T22:45
title: int
author: AlbertoVf
tags: ['variables', 'numérico', 'entero']
resume: Tipo de dato `int` en Python
---

# Tipo `int` en Python

En Python, el tipo `int` se utiliza para representar números enteros. Puedes crear variables de tipo `int` asignando un valor entero a una variable.

```python
# Ejemplo de variable int
numero_entero = 42
```

> Creación de variables `int`

## Operaciones con `int`

```python
# Suma
resultado_suma = 10 + 5
# Resta
resultado_resta = 20 - 8
# Multiplicación
resultado_multiplicacion = 6 * 7
# División
resultado_division = 15 / 3
# Potencia
resultado_potencia = 2 ** 3
```

> Operaciones basicas con int

```python
# Asignación simple
x = 10
# Asignación con operación aritmética
x += 5  # Equivalente a x = x + 5
# Otras operaciones: -=, *=, /=
```

> Operacion de asignacion

```python
# Convertir a int desde otro tipo
valor_string = "123"
valor_entero = int(valor_string)

# Convertir a int desde float
valor_float = 3.14
valor_entero_desde_float = int(valor_float)
```

> Conversor de tipos

## Funciones y métodos relacionados con `int`

```python
# Obtener el valor absoluto
valor_absoluto = abs(-7)

# Redondear hacia abajo
redondeo_abajo = int(9.99)

# Otras funciones: max(), min(), pow(base, exponente)
```

```python
es_int = isinstance(42, int)
```

> Verificar si una variable es de tipo int
