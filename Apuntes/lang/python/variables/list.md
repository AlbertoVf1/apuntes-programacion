---
date: 2023-12-21T22:33
title: variable list
author: AlbertoVf
tags: ['variables', 'colección', 'lista']
resume: Tipo `list` en Python
---

# Tipo `list` en Python

El tipo `list` en Python es una estructura de datos que permite almacenar una colección ordenada de elementos, que pueden ser de diferentes tipos. Las listas son mutables, lo que significa que puedes cambiar, añadir o eliminar elementos después de crear la lista.

- Las listas son mutables, lo que significa que puedes modificarlas después de su creación.
- Las operaciones en listas, como `append()` y `remove()`, modifican la lista original.

## Creacion de `list`

```python
mi_lista = [1, 2, 3, "cuatro", 5.0]
```

> Usando Corchetes

```python
otra_lista = list("Python")  # Convierte una cadena en una lista de caracteres
```

> Con la Función `list()`

```python
lista_anidada = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
elemento = lista_anidada[0][1]  # Accede al segundo elemento de la primera lista
```

> Listas Anidadas: Puedes tener listas dentro de listas para crear estructuras más complejas.

## Acceso a elementos en `list`

```python
mi_lista = [10, 20, 30, 40, 50]
primer_elemento = mi_lista[0]  # 10
```

> Los elementos de una lista se pueden acceder mediante índices, comenzando desde 0.

```python
for elemento in mi_lista:
    print(elemento)
```

> Las listas se utilizan comúnmente en bucles `for` para iterar sobre sus elementos.

## Funciones y métodos relacionados con `list`

```python
mi_lista.append(60)  # Agrega 60 al final de la lista
```

> Agregar Elementos

```python
mi_lista.remove(20)  # Elimina el primer elemento con el valor 20
```

> Eliminar Elementos

```python
longitud = len(mi_lista)  # Devuelve la cantidad de elementos en la lista
```

> Obtener Longitud

```python
sublista = mi_lista[1:3]  # Obtiene elementos desde el índice 1 hasta el 2 (exclusivo)
```

> Slicing

```python
veces = mi_lista.count(30)  # Cuántas veces aparece el valor 30
```

> `count()`: Cuenta cuántas veces aparece un elemento en la lista.

```python
indice = mi_lista.index(40)  # Índice del valor 40
```

> `index()`: Devuelve el índice del primer elemento con el valor especificado.

```python
mi_lista.sort()  # Ordena la lista de forma ascendente
```

> `sort()`: Ordena la lista.
