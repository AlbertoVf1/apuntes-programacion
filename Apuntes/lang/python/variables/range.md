---
date: 2023-12-21T22:33
title: variable range
author: AlbertoVf
tags: ['variables', 'colección', 'lista', 'números']
resume: Tipo `range` en Python
---

# Tipo `range` en Python

El tipo `range` en Python es utilizado para representar secuencias de números inmutables. Es comúnmente utilizado en bucles `for` para iterar sobre un rango de valores.

## Creacion de `range`

- El rango es inclusivo en el inicio pero exclusivo en el final.
- Los objetos `range` son eficientes en términos de memoria ya que generan valores sobre la marcha en lugar de almacenar todos los valores en la memoria.
- El tipo de datos `range` es una herramienta poderosa para trabajar con secuencias de números en Python, especialmente en contextos donde la generación de números es más eficiente que almacenarlos en una lista.

```python
rango = range(5)  # Crea un rango de 0 a 4
```

> Sintaxis Básica

```python
rango_personalizado = range(2, 10, 2)  # Inicio en 2, fin en 10 (exclusivo), paso de 2
```

> Especificar Inicio, Fin y Paso

## Acceso a elementos en `range`

Los objetos `range` no son listas, pero se pueden convertir en listas o iterar directamente.

```python
rango = range(5)

# Convertir a lista
lista_del_rango = list(rango)  # [0, 1, 2, 3, 4]

# Iterar directamente
for numero in rango:
    print(numero)
```

Los objetos `range` no admiten indexación directa o slicing. Si necesitas acceder a elementos específicos, convierte el rango en una lista.

```python
rango = range(5)
elemento = list(rango)[2]  # Accede al tercer elemento (índice 2)
```

## Funciones y metodos relacionados con `range`

```python
for i in range(5):
    print(i)
# Salida: 0 1 2 3 4
```

> El uso más común de `range` es en bucles `for` para iterar sobre una secuencia de números.

```python
longitud = len(range(5))  # 5
```

> `len()`: Obtiene la longitud del rango.

```python
minimo = min(range(5))  # 0
maximo = max(range(5))  # 4
```

> `min()` y `max()`: Obtienen el valor mínimo y máximo del rango, respectivamente.
