---
date: 2023-12-21T22:31
title: variable set
author: AlbertoVf
tags: ['variables', 'colección', 'única']
resume: Tipo `set` en Python
---

# Tipo `set` en Python

El tipo `set` en Python es una colección no ordenada de elementos únicos. Los sets son útiles cuando se desea almacenar elementos sin necesidad de mantener un orden específico y garantizar que no haya duplicados.

- Los sets no garantizan un orden específico de los elementos.
- Los elementos de un set deben ser inmutables (no se pueden cambiar después de agregar al set).
- Los sets son eficientes para verificar la pertenencia y eliminar duplicados.

Los sets son una herramienta útil en Python cuando necesitas almacenar elementos únicos y realizar operaciones de conjuntos de manera eficiente.

## Creacion de `set`

```python
mi_set = {1, 2, 3, 4, 5}
```

> Usando llaves `{}`

```python
otro_set = set([2, 4, 6, 8, 10])
```

> Con la Función `set()`

## Operaciones de Conjuntos

Los sets admiten operaciones comunes de conjuntos, como unión, intersección y diferencia.

```python
set1 = {1, 2, 3, 4, 5}
set2 = {3, 4, 5, 6, 7}

# Unión
union_set = set1 | set2  # {1, 2, 3, 4, 5, 6, 7}

# Intersección
interseccion_set = set1 & set2  # {3, 4, 5}

# Diferencia
diferencia_set = set1 - set2  # {1, 2}
```

```python
union_set = set1.union(set2)
```

> `union()`: Retorna la unión de dos sets.

```python
interseccion_set = set1.intersection(set2)
```

> `intersection()`: Retorna la intersección de dos sets.

```python
diferencia_set = set1.difference(set2)
```

> `difference()`: Retorna la diferencia entre dos sets.

```python
mi_frozenset = frozenset([1, 2, 3, 4, 5])
```

> Los sets también pueden ser inmutables, utilizando el tipo de dato `frozenset`. A diferencia de los sets, los frozensets no pueden ser modificados después de su creación.

```python
mi_lista = [1, 2, 2, 3, 4, 4, 5]
mi_set_sin_duplicados = set(mi_lista)  # Elimina duplicados
```

> Los sets son útiles para eliminar duplicados de una lista y para realizar operaciones de conjuntos de manera eficiente.

## Funciones y métodos relacionados con `set`

```python
mi_set.add(6)  # Agrega el elemento 6 al set
```

> Agregar Elementos

```python
mi_set.remove(3)  # Elimina el elemento 3 del set
```

> Eliminar Elementos

```python
pertenece = 5 in mi_set  # Verifica si 5 está en el set
```

> Verificación de Pertenencia
