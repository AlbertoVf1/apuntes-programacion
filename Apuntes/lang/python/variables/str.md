---
date: 2023-12-20T22:50
title: str
author: AlbertoVf
tags: ['variables', 'cadena', 'texto']
resume: Documento sobre el tipo `str` en Python
---

# Tipo `str` en Python

El tipo `str` se utiliza para representar cadenas de caracteres en Python. A continuación, se presenta un documento detallado con ejemplos sobre el tipo `str`:

```python
# Ejemplo de variable str
mensaje = "Hola, mundo!"
```

## Operaciones con `str`

```python
saludo = "Hola"
nombre = "Juan"
mensaje_completo = saludo + ", " + nombre + "!"
```

> Concatenación de cadenas

```python
# Acceso a caracteres mediante índices
primer_caracter = mensaje[0]  # "H"
# Segmentación de cadenas
subcadena = mensaje[0:5]  # "Hola"
```

## Funciones y métodos relacionados con `str`

```python
# Longitud de la cadena
longitud = len(mensaje)
# Convertir a minúsculas y mayúsculas
en_minusculas = mensaje.lower()
en_mayusculas = mensaje.upper()

# Reemplazar texto
nuevo_mensaje = mensaje.replace("Hola", "Saludos")

# Encontrar la posición de un subtexto
posicion = mensaje.find("mundo")
```

```python
es_str = isinstance("Python", str)
```

> Verificar si una variable es de tipo str

```python
# Formateo con placeholders
nombre = "Ana"
edad = 25
saludo_formateado = "Hola, {}! Tienes {} años.".format(nombre, edad)
# Formateo con f-strings (a partir de Python 3.6)
saludo_fstring = f"Hola, {nombre}! Tienes {edad} años."
```

## Caracteres de escape

```python
mensaje_multilinea = "Hola,\n¿Cómo estás?\n"
```

> Uso de caracteres de escape
