---
date: 2023-12-20T21:48
title: tipos de variables
author: AlbertoVf
tags: ['python', 'variables', 'declaración']
resume: Tipos de variables y ejemplos
---

# Tipos de Variables en Python

En Python, las variables son contenedores que se utilizan para almacenar valores. Python es un lenguaje de programación de tipado dinámico, lo que significa que no es necesario declarar explícitamente el tipo de una variable al asignarle un valor.

## Tipos de Variables Básicas

### Enteros (int)

Los enteros son números sin decimales.

```python
numero_entero = 10
otro_entero = -5
```

### Flotantes (float)

Los flotantes son números con decimales.

```python
numero_flotante = 3.14
otro_flotante = -0.5
```

### Cadenas de texto (str)

Las cadenas de texto son secuencias de caracteres.

```python
cadena_texto = "Hola, mundo!"
otra_cadena = 'Python es divertido'
```

## Tipos de Variables Avanzadas

### Listas

Las listas son secuencias ordenadas y modificables de elementos.

```python
mi_lista = [1, 2, 3, "cuatro", 5.0]
```

### Tuplas

Las tuplas son secuencias ordenadas e inmutables de elementos.

```python
mi_tupla = (1, 2, 3, "cuatro", 5.0)
```

### Conjuntos (set)

Los conjuntos son colecciones no ordenadas de elementos únicos.

```python
mi_conjunto = {1, 2, 3, 3, 4, 5}
```

### Diccionarios

Los diccionarios son colecciones no ordenadas de pares clave-valor.

```python
mi_diccionario = {'clave1': 'valor1', 'clave2': 2, 'clave3': [1, 2, 3]}
```

## Variables Booleanas

### Booleanas (bool)

Las variables booleanas representan los valores de verdad: Verdadero (`True`) o `Falso` (False).

```python
Copy code
verdadero = True
falso = False
```

## Variables Especiales

### Nulas (None)

La variable especial `None` se utiliza para representar la ausencia de un valor.

```python
valor_nulo = None
```
