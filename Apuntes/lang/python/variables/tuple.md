---
date: 2023-12-21T22:31
title: variable tuple
author: AlbertoVf
tags: ['variables', 'colección', 'inmutable']
resume: Tipo `tuple` en Python
---

# Tipo `tuple` en Python

El tipo `tuple` en Python representa una secuencia inmutable de elementos. A diferencia de las listas, los tuples no pueden ser modificados después de su creación. Son útiles cuando se necesita una colección ordenada de elementos que no cambiará durante la ejecución del programa.

Los tuples son útiles cuando necesitas datos inmutables, como claves en diccionarios o elementos en sets. También se usan para devolver múltiples valores desde una función.

- La inmutabilidad de los tuples los hace más eficientes en términos de memoria que las listas.
- Los tuples son hashables, lo que los hace aptos como claves en diccionarios y elementos en sets.

## Creacion de `tuple`

```python
mi_tuple = (1, 2, "tres", 4.0)
```

> Usando Paréntesis `()` Directamente

```python
otro_tuple = tuple([5, 6, 7, 8])
```

> Con la Función `tuple()`

## Inmutabilidad de Tuples

A diferencia de las listas, los tuples son inmutables. Una vez creado, no puedes modificar sus elementos ni agregar ni eliminar elementos.

```python
# Esto generará un error
mi_tuple[0] = 10
```

```python
primer_elemento = mi_tuple[0]  # Accede al primer elemento del tuple
```

> Los elementos en un tuple se acceden mediante índices, comenzando desde 0.

## Acceso a elementos en `tuple`

```python
for elemento in mi_tuple:
    print(elemento)
```

> Puedes iterar sobre los elementos de un tuple utilizando bucles `for`.

## Funciones y métodos relacionados con `tuple`

```python
nuevo_tuple = mi_tuple + otro_tuple
```

> Concatenación de Tuples

```python
tuple_repetido = mi_tuple * 3
```

> Repetición de Tuples

```python
longitud = len(mi_tuple)
```

> `len()`: Obtiene la longitud de un tuple.

```python
veces = mi_tuple.count(2)
```

> `count()`: Cuenta cuántas veces aparece un elemento en el tuple.

```python
indice = mi_tuple.index("tres")
```

> `index()`: Devuelve el índice del primer elemento con el valor especificado.

```python
a, b, c, d = mi_tuple
```

> Puedes desempaquetar los elementos de un tuple en variables individuales.

```python
def obtener_coordenadas():
    return (10, 20)

x, y = obtener_coordenadas()
```

> Devolviendo múltiples valores desde una función
