---
date: 2023-07-23T16:00
title: claves ssh
author: AlbertoVf
tags: ['terminal', 'comando', 'git']
resume: Como generar claves ssh y usarla en github
---

# Claves SSH

## Comprobar las claves ssh existentes

```bash
# Lista los ficheros del directorio .ssh si existe
$ ls -al ~/.ssh
```

## Como generar una clave ssh

Para generar una clave ssh realiza el siguiente comando

```bash
# <comando> -t <tipo_clave> -C <comentario>
ssh-keygen -t ed25519 -C "your_email@example.com"
```

Una vez realizado te pedirá el nombre del fichero en el que se guardará y una contraseña. Tras crear el fichero hay que activarlo por lo que ejecutaremos el comando.

> La clave se guardara en dos ficheros: uno con clave publica y otro con clave privada.

El fichero `<nombre>.pub` guardara la clave publica y el fichero `<nombre>` la clave privada que no se debe compartir

```bash
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/<nombre>
```

```bash
# copia el contenido del fichero al portapapeles
xclip -sel clip <clave-publica>
```

## Agregar clave ssh a github

1. Seleccionar `Settings` en el perfil
    ![Seleccionar Settings](claves-ssh_1.jpg)
2. Seleccionar `SSH and GPG keys` para añadir una nueva
    ![Seleccionar ssh](claves-ssh_2.jpg)
3. Pulsa New `SSH Key` para agregar una nueva
    ![Agregar clave ssh](claves-ssh_3.jpg)
4. Crear nueva clave. En title **indicar** un titulo para la clave y el tipo de clave. Copiar la clave publica en el campo `key`, pulsar botón para agregar
    ![Configurar clave ssh](claves-ssh_4.jpg)
