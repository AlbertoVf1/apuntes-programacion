---
date: 2023-04-14T16:12
title: Crea tus propios dotfiles
author: AlbertoVf
tags: ['linux', 'terminal', 'tutorial', 'personalizacion', 'git', 'configuracion', 'entorno']
resume: crear repositorio para los archivos de configuracion de linux
---

# Crea Tus Propios Dotfiles

[Dotfiles Tutorial](https://www.atlassian.com/git/tutorials/dotfiles)

```bash
git init --bare $HOME/dotfiles
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
dotfiles config --local status.showUntrackedFiles no
echo "alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'" >> $HOME/.bash
```

## Añade ficheros al repositorio

```bash
dotfiles status
dotfiles add <your-file>
dotfiles commit -m "<message>"
dotfiles push
```

## Instala los dotfiles en un nuevo sistema

```bash
echo "alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'" >> $HOME/.bash
echo ".dotfiles" >> .gitignore
git clone --bare <git-repo-url> $HOME/.dotfiles
dotfiles config --local status.showUntrackedFiles no
```
