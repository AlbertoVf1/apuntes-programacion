---
date: 2023-07-23T09:42
title: Crear Red Wifi en Linux
author: AlbertoVf
tags: ['personalización', 'Linux', 'tutorial', 'wifi', 'red personal']
resume: crear red wifi en Linux como punto de acceso
---

# Crear Red Wifi En Linux

## ¿Cómo Crear un sencillo y funcional Punto Caliente (Hotspot) sobre Linux?

Sin importar que **Sistema Operativo** pueda estar instalado sobre algún **Ordenador (de escritorio, móvil o portátil)**, con **conexión cableada e inalámbrica**, seguramente en algún momento su usuario/dueño habrá querido crear/configurar sobre el mismo un **Punto de acceso caliente**, es decir, un **Hotspot** sobre él. Y así aprovechar, de compartir su **Conexión de Internet** cableada a través de su **interfaz de red inalámbrica** con otros dispositivos. En cualquier caso, ya sea con **Windows, MacOS o Linux** se puede hacer un **Hotspot** vía **Interfaz de Línea de Comando (CLI)**, es decir, un terminal o consola . Sin embargo, en **Windows y MacOS** hay muchas aplicaciones de terceros que permiten crear un **Hotspot** de forma sencilla y funcional vía **Interfaz Gráfica (GUI)**. Sobre **Linux** no abundan las mismas, pero fácilmente se puede crear uno utilizando el **Gestor de conexiones de red** de su respectivo **Entorno de Escritorio**.

Primeramente, tengamos presente que un **Punto de acceso caliente o Hotspot** es básicamente un **acceso de cobertura** que se ofrece a través de un dispositivo para que haya una **comunicación inalámbrica**, sin cables, es decir, vía **conexión Wi-Fi**, para principalmente ofrecer **Conexión a Internet**, u otros recursos y servicios vía **red local (LAN)** o **red nacional o mundial (MAN/WAN)**.

Y en segundo lugar, que el procedimiento puede variar ligeramente dependiendo del respectivo **Entorno de Escritorio** (**Gnone, Plasma, KDE, XFCE, Cinnamon, Mate**, entre otros) utilizado sobre su **Distro GNU/Linux**.

## Procedimiento

### Paso 1

Abrir el **Gestor de conexiones de red** de su **Distribución GNU/Linux:**

![Menu edición de conexiones](crear-red-wifi-en-linux_1.jpg)

### Paso 2

Solicitar al **Gestor de conexiones de red** el añadir una nueva **conexiones de red** a la **Distribución GNU/Linux** usada:

![Conexiones disponibles](crear-red-wifi-en-linux_2.jpg)

### Paso 3

Indicarle al **Gestor de conexiones de red** que la nueva **conexiones de red** será del tipo **Inalámbrica**:

![Agregar conexión](crear-red-wifi-en-linux_3.jpg)

### Paso 4

Editar los parámetros de la nueva **conexiones de red**:

![Editar conexión](crear-red-wifi-en-linux_4.jpg)

En la pestaña llamada **Inalámbrica** de esta nueva ventana llamada **Editando Conexión inalámbrica 1** los siguientes campos deben ser rellenados con las respectivas recomendaciones sugeridas:

- **Nombre de la Conexión:** Hotspot (O el de su elección)
- **SSID:** Hotspot (O el de su elección)
- **Modo:** Hotspot o Punto de Acceso
- **Banda:** Automático
- **Canal:** Predeterminado
- **Dispositivo:** Wlan0 ( O el disponible)
- **MTU:** Automático

![Editar seguridad de red"](crear-red-wifi-en-linux_5.jpg)

En la pestaña llamada **Seguridad inalámbrica** los siguientes campos deben ser rellenados con las respectivas recomendaciones sugeridas:

- **Seguridad:** WPA y WPA2 Personal
- **Contraseña:** Hotspot\*2019 (O la de su elección)

![Ajustar IP](crear-red-wifi-en-linux_6.jpg)

Y en la pestaña llamada **Ajustes de IPV4** los siguientes campos deben ser rellenados con las respectivas recomendaciones sugeridas:

- **Método:** Compartida con otros equipos

![Mostrar red](crear-red-wifi-en-linux_7.jpg)

De resto solo falta, presionar el botón llamado **Guardar** para finalizar la configuración y proceder a probar el **Punto de Acceso** o **Hotspot. Recuerde que mientras el ordenador donde creo el**Hotspot**tenga el mismo activado, no mostrará las conexiones entrantes de**Wi-Fi**detectadas y disponibles. Una vez se desconecte del mismo se verán nuevamente las**Redes inalámbricas\*\*, tal como se muestran a continuación:

![Seleccionar red](crear-red-wifi-en-linux_8.jpg)
