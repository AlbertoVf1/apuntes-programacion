---
date: 2023-07-13T08:00
title: instalar archlinux
author: AlbertoVf
tags: ['linux', 'terminal', 'tutorial', 'arch-linux', 'instalacion']
resume: instalacion de archilux a traves de archinstall
---

# Instalar Archlinux Fácilmente

**Arch Linux** es una distribución que siempre ha intentado mantenerse fiel a la filosofía KISS, pero a la vez es la más conocida de entre las que van dirigidas a los usuarios avanzados. De hecho, la intención de los desarrolladores de mantener a Arch Linux como un sistema operativo KISS y altamente personalizable dio un giro de tuerca en 2012, cuando decidieron [eliminar el instalador](https://www.muylinux.com/2012/08/01/arch-linux-instalador/) para sustituirlo por un proceso más manual apoyado en pacstrap.

La eliminación del instalador no gustó a todo el mundo, pero los responsables de la distribución se mantuvieron firmes y al final la instalación manual se estandarizó con algunos matices. Sin embargo, nueve años después, los responsables de Arch Linux han decidido **incluir en la última imagen del sistema un [instalador oficial](https://archlinux.org/news/installation-medium-with-installer/) simplificado** para aquellos usuarios a los que se les atraganta la instalación manual (aunque con un poco de práctica tampoco es que sea tan difícil).

El instalador se llama “**archinstall**” y tiene como propósito facilitar la instalación de Arch Linux, si bien el proceso sigue siendo mediante la consola. No, aquí no hay interfaz semigráfica como la había en el instalador descontinuado en 2012, pero al menos hace el proceso bastante menos áspero para esos usuarios intermedios a los que Arch Linux les puede venir algo grande.

Dicho con otras palabras, lo que hace “[archinstall](https://archlinux.org/packages/extra/any/archinstall/)” es establecer mediante la consola de comandos unos pasos guiados para instalar Arch Linux sin complicaciones, pero sin acercarse a lo que ofrecen Debian, Ubuntu, Fedora y las muchas distribuciones que usan Calamares.

¿Y cuánto facilita realmente “[archinstall](https://wiki.archlinux.org/index.php/Installation_guide)” la instalación? Con el fin de dar toda la información, vamos a describir los pasos brevemente con una máquina virtual de VirtualBox.

## Instalando Arch Linux con “archinstall”

Para empezar hay que [descargar](https://archlinux.org/download/) la imagen de Arch Linux y arrancar el ordenador desde ella (el pendrive o DVD donde haya sido introducida) para iniciar el proceso de instalación. En el momento en que se pueda usar la consola hay que **introducir el comando “archinstall”**.

```shell
archinstall
```

![Iniciando archinstall en Arch Linux](instalar-archlinux-facilmente_01.jpg)

El primer paso de “archinstall” consiste en **establecer el idioma del teclado**, que en nuestro caso es “es” en referencia al español. Después de escribir “es” hay pulsar la tecla intro.

![Estableciendo el idioma del teclado en Arch Linux con archinstall](instalar-archlinux-facilmente_02.jpg)

El segundo paso consiste en **seleccionar la región**. Como MuyLinux es un portal español, hemos introducido el número 44 (España) y pulsado intro.

![Estableciendo la región en la instalación de Arch Linux con archinstall](instalar-archlinux-facilmente_03.jpg)

Luego hay que **elegir el medio** sobre el que se quiere instalar Arch Linux. En nuestro, la opción 1 (/dev/sda). Sobra decir que, si se quiere llevar esto sobre una máquina real de producción, hay que respaldar todos los datos personales en alguna unidad externa (un disco duro externo por ejemplo) antes de iniciar la instalación para evitar disgustos.

![Indicando la unidad donde se va a instalar Arch Linux](instalar-archlinux-facilmente_04.jpg)

Como cuarto paso tenemos la **selección del sistema de ficheros**. Aunque hemos seleccionar Btrfs, esto queda totalmente a elección del usuario, si bien recomendamos XFS en caso de usar un disco duro mecánico para mejorar las velocidades de lectura y escritura.

![Estableciendo el formato del sistema de ficheros para la instalación de Arch Linux con archinstall](instalar-archlinux-facilmente_05.jpg)

De manera opcional **se puede establecer una contraseña de cifrado**, algo que recomendamos encarecidamente si el equipo es un portátil que suele salir del hogar, porque a pesar de que GNU/Linux ofrece ciertas ventajas a nivel de seguridad frente a Windows, nada de eso sirve si las particiones donde se almacenan los datos no están cifradas.

![Cifrado de disco con archinstall](instalar-archlinux-facilmente_06.jpg)

El establecimiento del **nombre del \*host\*** es un clásico que algunos sistemas como Fedora Workstation han eliminado (aunque se puede ajustar luego desde al configuración de GNOME).

![Nombre de host con archinstall](instalar-archlinux-facilmente_07.jpg)

Arch Linux es una distribución que quiere ofrecer flexibilidad total, así que el usuario puede optar por **habilitar root o no** (estableciendo o no una contraseña) o **crear o no un usuarios comunes** y hacerlos “sudoers”. Una vez más, no vamos a entrar en valoraciones.

![Creación de los usuarios con archinstall](instalar-archlinux-facilmente_08.jpg)

Y aquí llegamos a una de las partes más importantes para aquellos que buscan una instalación fácil: la **selección del escritorio**. Si se deja el campo en blanco y se pulsa sobre intro, se saltará el proceso de instalación del entorno de escritorio, pero en nuestro caso hemos elegido GNOME (opción 2).

![Selección del escritorio en Arch Linux con archinstall](instalar-archlinux-facilmente_09.jpg)

En el noveno paso hay que seleccionar los **drivers para la marca de la gráfica** presente en ordenador. Debido a que hemos usado VirtualBox, seleccionamos VMware (opción 6).

![Seleccionando el driver gráfico para Arch Linux](instalar-archlinux-facilmente_10.jpg)

Después el usuario **puede seleccionar los paquetes adicionales** que quiere instalar, los cuales tienen que separarse solo con espacios en blanco.

![Indicar paquetes adicionales en archinstall](instalar-archlinux-facilmente_11.jpg)

En la **interfaz de red** hemos seleccionado la Ethernet generada por VirtualBox (opción 1) y configurarla mediante DHCP.

![Seleccionando la interfaz de red con archinstall](instalar-archlinux-facilmente_12.jpg) ![Configurando la red en Arch Linux](instalar-archlinux-facilmente_13.jpg)

Como último paso hay que **introducir una zona horaria que sea válida** para el sistema. En nuestro caso es “Europe/Madrid”.

```shell
Europe/Madrid
```

![Zona horaria en Arch Linux](instalar-archlinux-facilmente_14.jpg)

Después de realizar los pasos pertinentes (que no tienen por qué ser iguales para todos los usuarios), se presenta el **resumen de la instalación** y la posibilidad de iniciar el proceso pulsando la tecla intro.

![Resumen de la instalación de archinstall](instalar-archlinux-facilmente_15.jpg) ![Instalando Arch Linux con archinstall](instalar-archlinux-facilmente_16.jpg)

Y con esto ya tenemos nuestro Arch Linux instalado mediante “archinstall”. Por lo que se ve, el proceso está totalmente guiado, así que el usuario no tiene por qué tener unos conocimientos muy avanzados para realizarlo, aunque aquellos que prefieran la [instalación manual mediante pacstrap](https://wiki.archlinux.org/index.php/Installation_guide) podrán seguir realizándola como siempre.

![Arch Linux con GNOME instalado con archinstall](instalar-archlinux-facilmente_17.jpg)
