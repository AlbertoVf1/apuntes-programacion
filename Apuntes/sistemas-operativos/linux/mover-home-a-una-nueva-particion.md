---
date: 2022-04-14T15:30
title: Mover home a una nueva particion
author: AlbertoVf
tags: ['linux', 'terminal', 'tutorial', 'personalizacion', 'particiones', 'home']
resume: usar /home en una particion distinta
---

# Mover Home A Una Nueva Particion

```bash
sudo mkdir /mnt/tmp
sudo mount /dev/sdb1 /mnt/tmp
```

> Montar la nueva particion en **/tmp**

```bash
sudo rsync -avx /home/ /mnt/tmp
```

> Copia tu **Home** en el segundo disco

```bash
sudo mount /dev/sdb1 /home
```

> Monta la nueva particion **Home**

```bash
sudo umount /home
```

> Si todos los datos estan copiados en el segundo disco, borramos nuestro viejo **/home**. Para hacerlo, debemos desmontar la nueva particion!

```bash
rm -rf /home/*
```

> Borra el **/home** original

```bash
sudo blkid
```

> Ahora haremos nuestro nuevo /home permanente. Conseguimos el **UUID** de la particion asi que la añadimos al archivo **fstab**

```bash
sudo vim /etc/fstab
UUID=<numero desde above> /home ext4 defaults 0 2
```

> Editamos **fstab** y añadimos la linea:

```bash
sudo reboot
```

> Reinicia el equipo
