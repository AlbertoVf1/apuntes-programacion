# Mis apuntes de programación

Colección de apuntes de programación: Sistemas operativos, documentación, lenguajes, terminal, buenas practicas ...

## Documentación

- [Apuntes de Git](Apuntes/documentacion/git/apuntes-git.md)

- [Apuntes de Markdown](Apuntes/documentacion/markdown/apuntes-markdown.md)

## Editores

- [Apuntes de Visual Studio Code](Apuntes/editores/vs-code/visual-studio-code.md)

## Linux

- [Crear Clave SSH](Apuntes/sistemas-operativos/linux/claves-ssh/claves-ssh.md)
- [Crear Red wifi](Apuntes/sistemas-operativos/linux/crear-red-wifi-en-linux/crear-red-wifi-en-linux.md)
- [Instalar Arch Linux](Apuntes/sistemas-operativos/linux/instalar-archlinux-facilmente/instalar-archlinux-facilmente.md)
- [Crear dotfiles](Apuntes/sistemas-operativos/linux/crea-tus-propios-dotfiles.md)
- [Mover carpeta /home](Apuntes/sistemas-operativos/linux/mover-home-a-una-nueva-particion.md)
